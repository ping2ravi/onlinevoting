<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Vote for Delhi and Be part of Democracy</title>

<meta name="og:image" property="og:image" content="https://lh3.googleusercontent.com/-kjbDd9TN4d8/UotFoXLWOLI/AAAAAAAANrE/nxdLkt8GnXA/s720/evm.jpg"/>
<meta name="og:image:width" property="og:image:width" content="990" />
<meta name="og:title"  property="og:title" content="Vote for - Delhi and Be part of Democracy"/>
<meta name="og:url" property="og:url" content="http://www.vote4delhi.com/vote/home"/>
<meta name="og:site_name" property="og:site_name" content="vote4delhi.com"/>
<meta name="og:type" property="og:type" content="website"/>
<meta name="og:description" property="og:description" content="India's biggest online voting system for Delhi Election Now you can vote for Party/Candidate by their Assembly Constituency and see all kinds of stats in realtime. Voting is transparent and Right to Recall is core part of it, it means you can change your vote any time."/>
<meta name="fb:app_id" property="fb:app_id" content="618283854899839"/>


<link rel="stylesheet" href="<c:out value="${context}" />/css/<c:out value="${version}" />/reset.css" />
<link rel="stylesheet" href="<c:out value="${context}" />/css/<c:out value="${version}" />/text.css" />
<link rel="stylesheet" href="<c:out value="${context}" />/css/<c:out value="${version}" />/960_24_col.css" />
<link rel="stylesheet" href="<c:out value="${context}" />/css/<c:out value="${version}" />/demo.css" />
<link rel="stylesheet" href="<c:out value="${context}" />/css/<c:out value="${version}" />/style.css" />

<!-- jQuery library (served from Google) -->
<script src="<c:out value="${context}" />/js/<c:out value="${version}" />/jquery-1.9.0.min.js"></script>
<!-- bxSlider Javascript file -->
<script src="<c:out value="${context}" />/js/<c:out value="${version}" />/jquery.bxslider.min.js"></script>
<!-- bxSlider CSS file -->
<link href="<c:out value="${context}" />/css/<c:out value="${version}" />/jquery.bxslider.css" rel="stylesheet" />

<script src="<c:out value="${context}" />/js/<c:out value="${version}" />/jquery.timeago.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() {
	  jQuery("abbr.timeago").timeago();
	});
</script>
<style type="text/css">
	div.inline { float:right; color: #aaaaaa;text-shadow: activeborder; }
	.clearBoth { clear:both; }
</style>


<script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);

      function drawChart() {
    	  drawDelhiTotalVotersChart();
    	  drawPartyVoteChart();
    	  /*
    	  drawPartyBarChart();
    	  drawPrivatePublicVoteChart();
    	  drawPrivatePublicBarChart();
    	  drawGenderBarChart();
    	  drawGenderPieChart();
    	  //drawPartyVoteTable();
    	  */
      }
      function drawDelhiTotalVotersChart() {
          var data = google.visualization.arrayToDataTable([
            ['', 'Total Voters', 'Voted Last Time', 'Online Users'],
            ['',  12100000, 6000000, 5500000],
          ]);

          legend = new Object();
          textStyle = new Object();
          textStyle.color = 'black';
          legend.textStyle = textStyle;
          legend.position='top';
          legend.maxLines = 5;
            
          var options = {
            title: 'Delhi Voter Stats',
            backgroundColor: 'transparent',
            vAxis: {title: 'Voters',  titleTextStyle: {color: 'red'}},
            'legend': legend
          };

          var chart = new google.visualization.BarChart(document.getElementById('chart_delhi_stat_div'));
          chart.draw(data, options);
        }
      <% int count=0; %>
      function drawPartyVoteTable() {
          var data = new google.visualization.DataTable();
          data.addColumn('string', 'Party Name');
          data.addColumn('string', 'Votes');
          data.addRows([
			<c:forEach items="${partyVotelist}" var="onePartyVote">
			<% if(count > 0) {%>
			,
			<% } %>
			['<c:out value="${onePartyVote.partyName}" />', '<c:out value="${onePartyVote.totalVotes}" />']
			<% count++; %>
			</c:forEach>
          ]);

          var table = new google.visualization.Table(document.getElementById('party_vote_table_div'));
          table.draw(data, {showRowNumber: true});
        }
      
      function drawPartyVoteChart() {
        var data = google.visualization.arrayToDataTable([
        ['Party', 'Votes']
        <c:forEach items="${partyVotelist}" var="onePartyVote">
    	,['<c:out value="${onePartyVote.partyName}" />', <c:out value="${onePartyVote.totalVotes}" />]
	    </c:forEach>
	    
        ]);

        tooltip = new Object();
        tooltip.text = 'percentage';
        tooltip.trigger = 'focus';
        tooltip.showColorCode=true;

        legend = new Object();
        textStyle = new Object();
        textStyle.color = 'black';
        legend.textStyle = textStyle;
        legend.position='top';
        legend.maxLines = 5;

        titleTextStyle = new Object();
        titleTextStyle.color = 'orange';
        titleTextStyle.fontSize = 20;
        titleTextStyle.backgroundColor = 'black';
        
        var options = {
          title: 'Party Wise Votes',
          is3D: true,
          tooltip:tooltip,
          backgroundColor: 'transparent',
          color: 'white',
          'legend': legend,
          'titleTextStyle' : titleTextStyle,
        };
        //'legend': { 'textStyle': { 'color': 'white' } }

        var chart = new google.visualization.PieChart(document.getElementById('partyvotes_piechart_3d'));
        chart.draw(data, options);
      }

      function drawPrivatePublicVoteChart() {
          var data = google.visualization.arrayToDataTable([
          ['Type', 'Votes']
          
	  	<c:forEach items="${publicPrivateVote}" var="oneVote">
      	,['<c:out value="${oneVote.description}" />', <c:out value="${oneVote.totalVotes}" />]
  	    </c:forEach>
  	    
          ]);

          tooltip = new Object();
          tooltip.text = 'percentage';
          tooltip.trigger = 'focus';
          tooltip.showColorCode=true;
          var options = {
            title: 'Party wise Votes',
            is3D: true,
            tooltip:tooltip,
          };

          var chart = new google.visualization.PieChart(document.getElementById('privatepublicvotes_piechart_3d'));
          chart.draw(data, options);
        }

      function drawPrivatePublicBarChart() {
          var data = google.visualization.arrayToDataTable([
            ['Votes', 'Gender']
    	  	<c:forEach items="${publicPrivateVote}" var="oneVote">
          	,['<c:out value="${oneVote.description}" />', <c:out value="${oneVote.totalVotes}" />]
      	    </c:forEach>
          ]);

          tooltip = new Object();
          tooltip.text = 'percentage';
          tooltip.trigger = 'focus';
          tooltip.showColorCode=true;
          var options = {
            title: 'Public/Anonymous Votes',
            tooltip : tooltip
          };

          var chart = new google.visualization.BarChart(document.getElementById('privatepublic_bar_chart'));
          chart.draw(data, options);
        }

      function drawPartyBarChart() {
          var data = google.visualization.arrayToDataTable([
            ['Votes', 'Party']
            <c:forEach items="${partyVotelist}" var="onePartyVote">
        	,['<c:out value="${onePartyVote.partyName}" />', <c:out value="${onePartyVote.totalVotes}" />]
    	    </c:forEach>
          ]);

          tooltip = new Object();
          tooltip.text = 'percentage';
          tooltip.trigger = 'focus';
          tooltip.showColorCode=true;
          var options = {
            title: 'Party Wise Votes',
            tooltip : tooltip
          };

          var chart = new google.visualization.BarChart(document.getElementById('party_bar_chart'));
          chart.draw(data, options);
        }

      function drawGenderPieChart() {
          var data = google.visualization.arrayToDataTable([
          ['Type', 'Votes']
          
  	  		<c:forEach items="${genderVotes}" var="oneVote">
        	,['<c:out value="${oneVote.description}" />', <c:out value="${oneVote.totalVotes}" />]
    	    </c:forEach>
  	    
          ]);

          tooltip = new Object();
          tooltip.text = 'percentage';
          tooltip.trigger = 'focus';
          tooltip.showColorCode=true;
          var options = {
            title: 'Gender Votes',
            is3D: true,
            tooltip:tooltip,
          };

          var chart = new google.visualization.PieChart(document.getElementById('gendervotes_piechart_3d'));
          chart.draw(data, options);
        }
      
      function drawGenderBarChart() {
          var data = google.visualization.arrayToDataTable([
            ['Votes', 'Gender']
    	  	<c:forEach items="${genderVotes}" var="oneVote">
          	,['<c:out value="${oneVote.description}" />', <c:out value="${oneVote.totalVotes}" />]
      	    </c:forEach>
          ]);

          tooltip = new Object();
          tooltip.text = 'percentage';
          tooltip.trigger = 'focus';
          tooltip.showColorCode=true;
          var options = {
            title: 'Gender Wise Votes',
            tooltip : tooltip
          };

          var chart = new google.visualization.BarChart(document.getElementById('gender_bar_chart'));
          chart.draw(data, options);
        }
      
    </script>
    
</head>

<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&amp;appId=618283854899839";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="outerWrapper">
	<div class="">
	<!-- menu starts -->
    <div class="menuWrapper">
    <div class="container_24">
    <div class="grid_24 mainMenu">
    <div class="grid_17 push_7 omega ">
        	<div class="menu">
            	<ul>
                <li><a href="<c:out value="${context}" />/">Home</a></li>
                <li><a href="<c:out value="${context}" />/myconstituency">My Constituency</a></li>
                <li><a href="<c:out value="${context}" />/profile">Profile</a></li>
                <li><a href="<c:out value="${context}" />/stats/delhi">Delhi Stats</a></li>
                <c:if test="${empty LoggedInUser}">
                <li><a href="<c:out value="${context}" />/login/facebook?v4d_redirect_url=<c:out value="${CurrentUrl}" />">Login</a></li>
                </c:if>
                <c:if test="${!empty LoggedInUser}">
                <li><a href="<c:out value="${context}" />/logout?v4d_redirect_url=<c:out value="${CurrentUrl}" />">Logout</a></li>
                </c:if>
              </ul>
            </div>
        </div>
        <div class="clear"></div>
    </div>
    
        <div class="clear"></div>
    </div>
    </div>
    <!-- menu ends -->
    
    <!-- header starts -->
    <div class="headerWrapper">
    <div class="container_24">
    <div class="grid_24 header panel">
    	<div class="grid_7 alpha">
        	<div class="logo"><a href="#"><img src="<c:out value="${context}" />/images/<c:out value="${version}" />/index.png" width="179px" /></a></div>
        	<br></br>
        	<div class="fb-like" data-href="http://www.vote4delhi.com/vote/home" data-width="200" data-show-faces="true" data-send="true"></div>
        	<br></br>
        	<a href="https://twitter.com/share" class="twitter-share-button" data-lang="en" data-url="http://www.vote4delhi.com/vote/home" data-text="Now vote on India's biggest online voting platform. #vote4delhi?" >Tweet Now</a>
		<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
        </div>
        <div class="grid_8"> 
        
        <div class="introContent">
        	<h5>Intro</h5>
            <p>India's biggest online voting system for Delhi Election</p>
            <p>Now you can vote for Party/Candidate by their Assembly Constituency and see all kinds of stats in realtime. Voting is transparent and Right to Recall is core part of it, it means you can change your vote any time.</p>
            <a href="<c:out value="${context}" />/myconstituency"><button  class="button voteNowBtn">Vote Now</button></a>
        </div>
        
        
        </div>
        <div class="grid_9 omega ">
        <h5>Delhi Voters Data</h5>
        <div id="chart_delhi_stat_div" style="width: 400px; height: 300px;"></div>
        
        <!--  
          <ul class="generalListing">
                        	<li><a href="#">Total Voters : 1,21,00,000</a></li>
                            <li><a href="#">Last time Total Polled Votes : 60,00,000 </a></li>
                            <li><a href="#">Total Online Users in Delhi : 50,00,000</a></li>
                            <li><a href="#">Neque porro quisquam est  </a></li>
                            <li><a href="#">Neque porro quisquam  </a></li>
                            <li><a href="#">Pporro quisquam est qui </a></li>
                            <li><a href="#">Neque porro  est qui </a></li>
                        </ul>
                        -->
        </div>
        
        <div class="clear"></div>
        <div class="recentlyVoted grid_8 push_7 row_sec">
        <h5>Recently Voted</h5>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
			<c:forEach items="${latestVotes}" var="oneVote">
				<tr>
				    <td width="15%" class="jusNow"><img width=40 src="<c:out value='${oneVote.profilePic}' />" /></td>
				    <td width="85%" class="location"><c:out value='${oneVote.votingMessage}' />
				    <div class="inline">
				    <c:if test="${oneVote.publicVote}">
				    <img src="https://cdn4.iconfinder.com/data/icons/pictype-free-vector-icons/16/view-20.png" title="public Vote"/>
				    </c:if>
				    <c:if test="${!oneVote.publicVote}">
				    	<img src="https://cdn0.iconfinder.com/data/icons/cosmo-medicine/40/blink-20.png" title="Anonymous Vote" />
				    </c:if>
				     
				    <abbr  class="timeago" title="<c:out value='${oneVote.votingTime}' />">${oneVote.votingTime}</abbr> </div>
				    
				    </td>
				</tr>
			</c:forEach>
</table>
        </div>
        <div class="grid_9 push_7 stats row_sec">
        <div class="holder">
        <h5>Statistics</h5>
        <div id="partyvotes_piechart_3d" style="width: 400px; height: 400px;"></div>
        <a href="/vote/stats/delhi"> More Stats...</a>
        <!-- 
        <img src="<c:out value="${context}" />/images/<c:out value="${version}" />/stats.jpg" />
         -->
        </div>
        </div>
    </div>
    </div>
    <div class="clear"></div>
    </div>
    <!-- header ends -->


    <!-- footer starts -->
    <div class="footerWrapper">
    <div class="container_24">
    <div class="grid_24 panel footer">
    	Copyright &copy; 2013 <a href="#">www.vote4delhi.com</a>. All Rights Reserved.
    </div>
    </div>
    <div class="clear"></div>
    </div>
    <!-- footer ends -->
    
</div>
</div>

<!-- AddThis Smart Layers BEGIN -->
<!-- Go to http://www.addthis.com/get/smart-layers to customize -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-528b626a7b5e7d93"></script>
<script type="text/javascript">
  addthis.layers({
    'theme' : 'transparent',
    'share' : {
      'position' : 'left',
      'numPreferredServices' : 5
    }  
  });
</script>
<!-- AddThis Smart Layers END -->

</body>
</html>
