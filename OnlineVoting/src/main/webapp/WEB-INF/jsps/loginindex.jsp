<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Login</title>
<style>
.google
{
    display: block;
width: 150px;
height: 50px;
background-image:url(https://lh6.googleusercontent.com/-0gJBlPxOa6A/Um00OEm4VII/AAAAAAAANog/7YWldqBAOJc/s600/login.png);
background-position: 0px -200px;
}
.yahoo
{
    display: block;
width: 150px;
height: 50px;
background-image:url(https://lh6.googleusercontent.com/-0gJBlPxOa6A/Um00OEm4VII/AAAAAAAANog/7YWldqBAOJc/s600/login.png);
background-position: 0px 50px;
}
.facebook
{
    display: block;
width: 150px;
height: 50px;
background-image:url(https://lh6.googleusercontent.com/-0gJBlPxOa6A/Um00OEm4VII/AAAAAAAANog/7YWldqBAOJc/s600/login.png);
background-position: 0px -100px;
}
.myspace
{
    display: block;
width: 150px;
height: 50px;
background-image:url(https://lh6.googleusercontent.com/-0gJBlPxOa6A/Um00OEm4VII/AAAAAAAANog/7YWldqBAOJc/s600/login.png);
background-position: 0px 250px;
}
.aol
{
    display: block;
width: 150px;
height: 50px;
background-image:url(https://lh6.googleusercontent.com/-0gJBlPxOa6A/Um00OEm4VII/AAAAAAAANog/7YWldqBAOJc/s600/login.png);
background-position: 0px 0px;
}
.twitter
{
display: block;
width: 150px;
height: 50px;
background-image:url(https://lh6.googleusercontent.com/-0gJBlPxOa6A/Um00OEm4VII/AAAAAAAANog/7YWldqBAOJc/s600/login.png);
background-position: 0px 150px;
}
</style>
</head>
<body>
<table>
<tr>
<th colspan="2">Login using one of the following service</th>
</tr>
<tr>
<td>
<a href="./facebook?v4d_redirect_url=<c:out value='${redirectUrl}' />"><div class="facebook"></div></a>

</td>
<td>
<a href="./twitter?v4d_redirect_url=<c:out value='${redirectUrl}' />"><div class="twitter"></div></a>
</td>
</tr>
</table>
</body>
</html>
