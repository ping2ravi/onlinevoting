<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<title>Vote4Delhi</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="" />
<meta name="author" content="" />

<!-- CSS -->
<link rel="stylesheet"
	href="http://fonts.googleapis.com/css?family=PT+Sans:400,700" />
<link rel="stylesheet"
	href="http://fonts.googleapis.com/css?family=Oleo+Script:400,700" />
<link rel="stylesheet" href="<c:out value="${context}" />/css/<c:out value="${version}" />/bootstrap.min.css" />
<link rel="stylesheet" href="<c:out value="${context}" />/css/<c:out value="${version}" />/style.css" />
<!-- Javascript -->
<script src="<c:out value="${context}" />/js/<c:out value="${version}" />/jquery-1.8.2.min.js"></script>
<script src="<c:out value="${context}" />/js/<c:out value="${version}" />/bootstrap.min.js"></script>
<script src="<c:out value="${context}" />/js/<c:out value="${version}" />/jquery.backstretch.min.js"></script>
<script src="<c:out value="${context}" />/js/<c:out value="${version}" />/scripts.js"></script>


<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
<script type="text/javascript">
jQuery(document).ready(function() {
	  jQuery("abbr.timeago").timeago();
	});
</script>
<script type="text/javascript">
            google.load("visualization", "1", {
                packages: ["corechart"]
            });
            google.setOnLoadCallback(drawChart);
            function drawChart() {
                var data = google.visualization.arrayToDataTable([
                    ['Vote Split', 'Party Wise'],
                    ['AAP', 24],
                    ['BJP', 22],
                    ['Congress',20],
                    ['Others', 12],
                    
                ]);
                var options = {
                    title: 'Party Wise Vote Split',
                    slices: [{color: '#b51c44'}, {color: '#ce4b27'}, {color: '#009600'}, {color: '#e88a05'}], animation:{
                     duration: 1000,
                     easing: 'out',
                   }

                };
                var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
                chart.draw(data, options);
            }
        </script>

<script type="text/javascript">
            google.load("visualization", "1", {packages: ["corechart"]});
            google.setOnLoadCallback(drawChart);
            function drawChart() {
                var data = google.visualization.arrayToDataTable([
                    ['Year','AAP', 'BJP', 'Congress','Others'],
                    ['2008', 0,20,35,10],
                    ['2014', 30,20,25,8]
                ]);
                var options = {
                    title: 'Party Wise Seat Split',
                    vAxis: {title: 'Year', titleTextStyle: {color: 'red'}},
                    series: [{color: '#b51c44'}, {color: '#ce4b27'}, {color: '#009600'}, {color: '#e88a05'}]
                };
                var chart = new google.visualization.BarChart(document.getElementById('chart_div2'));
                chart.draw(data, options);
            }
        </script>
<script>
function myFunction()
{
alert("Please do Login to Facebook!");
}
</script>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);

      function drawChart() {
    	  drawPartyVoteChart();
    	  /*
    	  drawPartyBarChart();
    	  drawPrivatePublicVoteChart();
    	  drawPrivatePublicBarChart();
    	  drawGenderBarChart();
    	  drawGenderPieChart();
    	  //drawPartyVoteTable();
    	  */
      }
      <% int count=0; %>
      function drawPartyVoteTable() {
          var data = new google.visualization.DataTable();
          data.addColumn('string', 'Party Name');
          data.addColumn('string', 'Votes');
          data.addRows([
			<c:forEach items="${partyVotelist}" var="onePartyVote">
			<% if(count > 0) {%>
			,
			<% } %>
			['<c:out value="${onePartyVote.partyName}" />', '<c:out value="${onePartyVote.totalVotes}" />']
			<% count++; %>
			</c:forEach>
          ]);

          var table = new google.visualization.Table(document.getElementById('party_vote_table_div'));
          table.draw(data, {showRowNumber: true});
        }
      
      function drawPartyVoteChart() {
        var data = google.visualization.arrayToDataTable([
        ['Party', 'Votes']
        <c:forEach items="${partyVotelist}" var="onePartyVote">
    	,['<c:out value="${onePartyVote.partyName}" />', <c:out value="${onePartyVote.totalVotes}" />]
	    </c:forEach>
	    
        ]);

        tooltip = new Object();
        tooltip.text = 'percentage';
        tooltip.trigger = 'focus';
        tooltip.showColorCode=true;

        legend = new Object();
        textStyle = new Object();
        textStyle.color = 'white';
        legend.textStyle = textStyle;

        titleTextStyle = new Object();
        titleTextStyle.color = 'white';
        titleTextStyle.fontSize = 20;
        titleTextStyle.backgroundColor = 'black';
        
        var options = {
          title: 'Party Wise Votes',
          is3D: true,
          tooltip:tooltip,
          backgroundColor: 'transparent',
          color: 'white',
          'legend': legend,
          'titleTextStyle' : titleTextStyle,
        };
        //'legend': { 'textStyle': { 'color': 'white' } }

        var chart = new google.visualization.PieChart(document.getElementById('partyvotes_piechart_3d'));
        chart.draw(data, options);
      }

      function drawPrivatePublicVoteChart() {
          var data = google.visualization.arrayToDataTable([
          ['Type', 'Votes']
          
	  	<c:forEach items="${publicPrivateVote}" var="oneVote">
      	,['<c:out value="${oneVote.description}" />', <c:out value="${oneVote.totalVotes}" />]
  	    </c:forEach>
  	    
          ]);

          tooltip = new Object();
          tooltip.text = 'percentage';
          tooltip.trigger = 'focus';
          tooltip.showColorCode=true;
          var options = {
            title: 'Party wise Votes',
            is3D: true,
            tooltip:tooltip,
          };

          var chart = new google.visualization.PieChart(document.getElementById('privatepublicvotes_piechart_3d'));
          chart.draw(data, options);
        }

      function drawPrivatePublicBarChart() {
          var data = google.visualization.arrayToDataTable([
            ['Votes', 'Gender']
    	  	<c:forEach items="${publicPrivateVote}" var="oneVote">
          	,['<c:out value="${oneVote.description}" />', <c:out value="${oneVote.totalVotes}" />]
      	    </c:forEach>
          ]);

          tooltip = new Object();
          tooltip.text = 'percentage';
          tooltip.trigger = 'focus';
          tooltip.showColorCode=true;
          var options = {
            title: 'Public/Anonymous Votes',
            tooltip : tooltip
          };

          var chart = new google.visualization.BarChart(document.getElementById('privatepublic_bar_chart'));
          chart.draw(data, options);
        }

      function drawPartyBarChart() {
          var data = google.visualization.arrayToDataTable([
            ['Votes', 'Party']
            <c:forEach items="${partyVotelist}" var="onePartyVote">
        	,['<c:out value="${onePartyVote.partyName}" />', <c:out value="${onePartyVote.totalVotes}" />]
    	    </c:forEach>
          ]);

          tooltip = new Object();
          tooltip.text = 'percentage';
          tooltip.trigger = 'focus';
          tooltip.showColorCode=true;
          var options = {
            title: 'Party Wise Votes',
            tooltip : tooltip
          };

          var chart = new google.visualization.BarChart(document.getElementById('party_bar_chart'));
          chart.draw(data, options);
        }

      function drawGenderPieChart() {
          var data = google.visualization.arrayToDataTable([
          ['Type', 'Votes']
          
  	  		<c:forEach items="${genderVotes}" var="oneVote">
        	,['<c:out value="${oneVote.description}" />', <c:out value="${oneVote.totalVotes}" />]
    	    </c:forEach>
  	    
          ]);

          tooltip = new Object();
          tooltip.text = 'percentage';
          tooltip.trigger = 'focus';
          tooltip.showColorCode=true;
          var options = {
            title: 'Gender Votes',
            is3D: true,
            tooltip:tooltip,
          };

          var chart = new google.visualization.PieChart(document.getElementById('gendervotes_piechart_3d'));
          chart.draw(data, options);
        }
      
      function drawGenderBarChart() {
          var data = google.visualization.arrayToDataTable([
            ['Votes', 'Gender']
    	  	<c:forEach items="${genderVotes}" var="oneVote">
          	,['<c:out value="${oneVote.description}" />', <c:out value="${oneVote.totalVotes}" />]
      	    </c:forEach>
          ]);

          tooltip = new Object();
          tooltip.text = 'percentage';
          tooltip.trigger = 'focus';
          tooltip.showColorCode=true;
          var options = {
            title: 'Gender Wise Votes',
            tooltip : tooltip
          };

          var chart = new google.visualization.BarChart(document.getElementById('gender_bar_chart'));
          chart.draw(data, options);
        }
      
    </script>
</head>

<body>


<div class="header">
	<div class="container">
<a class="home" href="" style="" rel="tooltip" >Home</a> <a class="settings"
	href="settings.html" style="" rel="tooltip" >Settings</a>
	
<!-- 
<div class="row">
<div class="logo span4">
<h1>
<a href="">LOGO HERE<span class="red"></span></a>
</h1>
</div>
<div class="links span8">
<a class="home" href="" style="" rel="tooltip" data-placement="bottom"
	data-original-title="Home">Home</a> <a class="settings"
	href="settings.html" style="" rel="tooltip" data-placement="bottom"
	data-original-title="Settings">Settings</a>
</div>

 <a href="index.html"><img src="images/v1/icon.png"
	style="height: 108px; margin-left: 10px; border-radius: 10px" /></a>
</div>
 -->
</div>

</div>
<table width="100%">
	<tr>
		<td>
			<table>
				<tr valign="top" style="height: 200px;">
					<td> 
						<div class="row">
							<div class="register span6">
								<a class="btn btn-primary"
									href="./myconstituency"
									style="padding: 40px 60px 40px 60px; margin-left: 60px;">Vote Now</a>
							</div>
					
						</div>
					</td>
				</tr>
				<tr>
					<td> 
						<div id="partyvotes_piechart_3d" style="width: 500px; height: 300px;"></div>
					</td>
				</tr>
			</table>
		</td>
		<td>
				<div class="row">
					<div style="margin-right: 100px;" align="right">
						<h3 style="color: white;">Delhi Wide Status</h3>
							<table border="2" id="example1" summary="Meeting Results">
								<tbody>
									<tr>
										<td>Total Voters</td>
										<td>121,00,000</td>
									</tr>
									<tr>
										<td>Last time Total Polled Votes</td>
										<td>60,00,000</td>
									</tr>
									<tr>
										<td>Total Internet Users in Delhi</td>
										<td>50,00,000</td>
									</tr>
									<!-- 
									<tr>
										<td>Voted so far</td>
										<td>10,00,000</td>
									</tr>
									 -->
								</tbody>
							</table>
							<table>
	<tr>
		<td> 
						<table class="cellpic" width="400">
							<tbody>
								<tr style="background-color: #333333; color: #FFFFFF;">
									<th style="border: 1px solid; background-color: #A80000;" colspan="2">
										Recent Votes in Delhi
									</th>
								</tr>
							<c:forEach items="${latestVotes}" var="oneVote">
								<tr>
									<td style="border: 1px solid;width:60px;padding: 0 0 0 0;margin: 0 0 0 0;">
										<img src="<c:out value='${oneVote.profilePic}' />" />
									</td>
									<td style="border: 1px solid;">
										<c:out value='${oneVote.votingMessage}' />
										<abbr class="timeago" title="<c:out value='${oneVote.profilePic}' />">July 17, 2008</abbr>
									</td>
								</tr>
							</c:forEach>

							</tbody>
						</table>
		</td>
	</tr>
</table>
					</div>
				</div>
		</td>
	</tr>
</table>



<div class="backstretch"
	style="left: 0px; top: 0px; overflow: hidden; margin: 0px; padding: 0px; height: 624px; width: 1349px; z-index: -999999; position: fixed;">
<img src="<c:out value="${context}" />/images/<c:out value="${version}" />/2.jpg"
	style="position: absolute; margin: 0px; padding: 0px; border: none; width: 1349px; height: 823.504215851602px; max-width: none; z-index: -999999; left: 0px; top: -99.752107925801px;"
	class="deleteable" />
	<img src=""
	style="position: absolute; display: none; margin: 0px; padding: 0px; border: none; width: auto; height: auto; max-width: none; z-index: -999999;" />
</div>
</body>
</html>

