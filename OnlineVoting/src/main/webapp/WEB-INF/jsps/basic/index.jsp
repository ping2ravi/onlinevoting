<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Login</title>
<style>
.google {
	display: block;
	width: 150px;
	height: 50px;
	background-image:
		url(https://lh6.googleusercontent.com/-0gJBlPxOa6A/Um00OEm4VII/AAAAAAAANog/7YWldqBAOJc/s600/login.png);
	background-position: 0px -200px;
}

.yahoo {
	display: block;
	width: 150px;
	height: 50px;
	background-image:
		url(https://lh6.googleusercontent.com/-0gJBlPxOa6A/Um00OEm4VII/AAAAAAAANog/7YWldqBAOJc/s600/login.png);
	background-position: 0px 50px;
}

.facebook {
	display: block;
	width: 150px;
	height: 50px;
	background-image:
		url(https://lh6.googleusercontent.com/-0gJBlPxOa6A/Um00OEm4VII/AAAAAAAANog/7YWldqBAOJc/s600/login.png);
	background-position: 0px -100px;
}

.myspace {
	display: block;
	width: 150px;
	height: 50px;
	background-image:
		url(https://lh6.googleusercontent.com/-0gJBlPxOa6A/Um00OEm4VII/AAAAAAAANog/7YWldqBAOJc/s600/login.png);
	background-position: 0px 250px;
}

.aol {
	display: block;
	width: 150px;
	height: 50px;
	background-image:
		url(https://lh6.googleusercontent.com/-0gJBlPxOa6A/Um00OEm4VII/AAAAAAAANog/7YWldqBAOJc/s600/login.png);
	background-position: 0px 0px;
}

.twitter {
	display: block;
	width: 150px;
	height: 50px;
	background-image:
		url(https://lh6.googleusercontent.com/-0gJBlPxOa6A/Um00OEm4VII/AAAAAAAANog/7YWldqBAOJc/s600/login.png);
	background-position: 0px 150px;
}
</style>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);

      function drawChart() {
    	  drawPartyVoteChart();
    	  drawPartyBarChart();
    	  drawPrivatePublicVoteChart();
    	  drawPrivatePublicBarChart();
    	  drawGenderBarChart();
    	  drawGenderPieChart();
    	  //drawPartyVoteTable();
      }
      <% int count=0; %>
      function drawPartyVoteTable() {
          var data = new google.visualization.DataTable();
          data.addColumn('string', 'Party Name');
          data.addColumn('string', 'Votes');
          data.addRows([
			<c:forEach items="${partyVotelist}" var="onePartyVote">
			<% if(count > 0) {%>
			,
			<% } %>
			['<c:out value="${onePartyVote.partyName}" />', '<c:out value="${onePartyVote.totalVotes}" />']
			<% count++; %>
			</c:forEach>
          ]);

          var table = new google.visualization.Table(document.getElementById('party_vote_table_div'));
          table.draw(data, {showRowNumber: true});
        }
      
      function drawPartyVoteChart() {
        var data = google.visualization.arrayToDataTable([
        ['Party', 'Votes']
        <c:forEach items="${partyVotelist}" var="onePartyVote">
    	,['<c:out value="${onePartyVote.partyName}" />', <c:out value="${onePartyVote.totalVotes}" />]
	    </c:forEach>
	    
        ]);

        tooltip = new Object();
        tooltip.text = 'percentage';
        tooltip.trigger = 'focus';
        tooltip.showColorCode=true;
        var options = {
          title: 'Party Votes',
          is3D: true,
          tooltip:tooltip,
        };

        var chart = new google.visualization.PieChart(document.getElementById('partyvotes_piechart_3d'));
        chart.draw(data, options);
      }

      function drawPrivatePublicVoteChart() {
          var data = google.visualization.arrayToDataTable([
          ['Type', 'Votes']
          
	  	<c:forEach items="${publicPrivateVote}" var="oneVote">
      	,['<c:out value="${oneVote.description}" />', <c:out value="${oneVote.totalVotes}" />]
  	    </c:forEach>
  	    
          ]);

          tooltip = new Object();
          tooltip.text = 'percentage';
          tooltip.trigger = 'focus';
          tooltip.showColorCode=true;
          var options = {
            title: 'Party Votes',
            is3D: true,
            tooltip:tooltip,
          };

          var chart = new google.visualization.PieChart(document.getElementById('privatepublicvotes_piechart_3d'));
          chart.draw(data, options);
        }

      function drawPrivatePublicBarChart() {
          var data = google.visualization.arrayToDataTable([
            ['Votes', 'Gender']
    	  	<c:forEach items="${publicPrivateVote}" var="oneVote">
          	,['<c:out value="${oneVote.description}" />', <c:out value="${oneVote.totalVotes}" />]
      	    </c:forEach>
          ]);

          tooltip = new Object();
          tooltip.text = 'percentage';
          tooltip.trigger = 'focus';
          tooltip.showColorCode=true;
          var options = {
            title: 'Public/Anonymous Votes',
            tooltip : tooltip
          };

          var chart = new google.visualization.BarChart(document.getElementById('privatepublic_bar_chart'));
          chart.draw(data, options);
        }

      function drawPartyBarChart() {
          var data = google.visualization.arrayToDataTable([
            ['Votes', 'Party']
            <c:forEach items="${partyVotelist}" var="onePartyVote">
        	,['<c:out value="${onePartyVote.partyName}" />', <c:out value="${onePartyVote.totalVotes}" />]
    	    </c:forEach>
          ]);

          tooltip = new Object();
          tooltip.text = 'percentage';
          tooltip.trigger = 'focus';
          tooltip.showColorCode=true;
          var options = {
            title: 'Party Wise Votes',
            tooltip : tooltip
          };

          var chart = new google.visualization.BarChart(document.getElementById('party_bar_chart'));
          chart.draw(data, options);
        }

      function drawGenderPieChart() {
          var data = google.visualization.arrayToDataTable([
          ['Type', 'Votes']
          
  	  		<c:forEach items="${genderVotes}" var="oneVote">
        	,['<c:out value="${oneVote.description}" />', <c:out value="${oneVote.totalVotes}" />]
    	    </c:forEach>
  	    
          ]);

          tooltip = new Object();
          tooltip.text = 'percentage';
          tooltip.trigger = 'focus';
          tooltip.showColorCode=true;
          var options = {
            title: 'Gender Votes',
            is3D: true,
            tooltip:tooltip,
          };

          var chart = new google.visualization.PieChart(document.getElementById('gendervotes_piechart_3d'));
          chart.draw(data, options);
        }
      
      function drawGenderBarChart() {
          var data = google.visualization.arrayToDataTable([
            ['Votes', 'Gender']
    	  	<c:forEach items="${genderVotes}" var="oneVote">
          	,['<c:out value="${oneVote.description}" />', <c:out value="${oneVote.totalVotes}" />]
      	    </c:forEach>
          ]);

          tooltip = new Object();
          tooltip.text = 'percentage';
          tooltip.trigger = 'focus';
          tooltip.showColorCode=true;
          var options = {
            title: 'Gender Wise Votes',
            tooltip : tooltip
          };

          var chart = new google.visualization.BarChart(document.getElementById('gender_bar_chart'));
          chart.draw(data, options);
        }
      
    </script>
</head>
<body>
<table>
		<tr>
			<td width="100"><a href="./home">Home</a></td>
			<td width="200"><a href="./myconstituency">My Constituency</a></td>
			<td width="100"><a href="./profile">My Profile</a></td>
		</tr>
	</table>


<table width="80%">
<tr valign="top">
<td>
<%
			int i=1;
		%>
		<h2>Select your constituency to vote</h2>
	<table>
	<tr style="background-color: #777777;border: solid 1px;width:500px;">
		<th style="border:1px solid;">S.No</th>
		<th style="border:1px solid;">Constituency Name</th>
		<th style="border:1px solid;">Vote</th>
	</tr>
	<c:forEach items="${aclist}" var="oneAc">
		<tr>
			<td style="border:1px solid;"><%=i%></td>
			<td style="border:1px solid;"><a
				href="./ac/delhi/<c:out value='${oneAc.urlName}' />" > <c:out value='${oneAc.name}' /></a></td>
			<td style="border:1px solid;"><a
				href="./ac/delhi/<c:out value='${oneAc.urlName}' />" > Vote Now</a></td>
		</tr>
		<%
			i++;
		%>
	</c:forEach>
	</table>
	
	
	
</td>
<td>
<%
		 i=1;
		%>
		<h2>Party Votes</h2>
		<table>
		<tr>
		<td>
		<!-- 
		<div id="party_vote_table_div" style="width: 500px; height: 500px;"></div>
		 -->
		<table>
	<tr valign="bottom">
		<td colspan="3"> * Party names are in alphabetical order </td>
	</tr>
	<tr style="background-color: #777777;">
		<th style="border:1px solid;">S.N.</th>
		<th style="border:1px solid;">Party Name</th>
		<th style="border:1px solid;">% of Votes</th>
	</tr>
	<c:forEach items="${partyVotelist}" var="onePartyVote">
		<tr style="border:1px solid;">
			<td style="border:1px solid;"><%=i%></td>
			<td style="border:1px solid;"><c:out value='${onePartyVote.partyName}' /></td>
			<td style="border:1px solid;"><fmt:formatNumber type="number" 
            maxFractionDigits="3" value="${onePartyVote.votePercent}" /></td>
		</tr>
		<%
			i++;
		%>
	</c:forEach>
	</table>
		</td>
		<td>
		<div id="partyvotes_piechart_3d" style="width: 500px; height: 500px;"></div>
		</td>
		<td>
		<div id="party_bar_chart" style="width: 500px; height: 500px;"></div>
		</td>
		
		</tr>
		</table>
	
	<table>
	<tr>
	<td>
	<table>
	<tr style="background-color: #777777;">
		<th style="border:1px solid;">Description</th>
		<th style="border:1px solid;">% of Votes</th>
	</tr>
	<c:forEach items="${publicPrivateVote}" var="oneVote">
		<tr>
			<td style="border:1px solid;"><c:out value='${oneVote.description}' /></td>
			<td style="border:1px solid;"><fmt:formatNumber type="number" 
            maxFractionDigits="3" value="${oneVote.votePercent}" /></td>
		</tr>
		<%
			i++;
		%>
	</c:forEach>
	</table>
	</td>
	<td>
	<div id="privatepublicvotes_piechart_3d" style="width: 500px; height: 500px;"></div>
	</td>
	<td>
	<div id="privatepublic_bar_chart" style="width: 500px; height: 300px;"></div>
	</td>
	
	</tr>
	</table>
	
	<table>
	<tr>
	<td>
	<table>
	<tr style="background-color: #333333;color: #FFFFFF;">
		<th style="border:1px solid;" colspan="2">Gender wise Votes</th>
	</tr>
	<tr style="background-color: #777777;">
		<th style="border:1px solid;">Gender</th>
		<th style="border:1px solid;">% of Votes</th>
	</tr>
	<c:forEach items="${genderVotes}" var="oneVote">
		<tr>
			<td style="border:1px solid;"><c:out value='${oneVote.description}' /></td>
			<td style="border:1px solid;"><fmt:formatNumber type="number" 
            maxFractionDigits="3" value="${oneVote.votePercent}" /></td>
		</tr>
		<%
			i++;
		%>
	</c:forEach>
	</table>
	</td>
	<td>
	<div id="gendervotes_piechart_3d" style="width: 500px; height: 500px;"></div>
	</td>
	<td>
	<div id="gender_bar_chart" style="width: 500px; height: 300px;"></div>
	</td>
	</tr>
	</table>
	
	
	<table>
	<tr style="background-color: #333333;color: #FFFFFF;">
		<th style="border:1px solid;" colspan="2">Age wise Votes</th>
	</tr>
	<tr style="background-color: #777777;">
		<th style="border:1px solid;">Age</th>
		<th style="border:1px solid;">% of Votes</th>
	</tr>
	<c:forEach items="${ageVotes}" var="oneVote">
		<tr>
			<td style="border:1px solid;"><c:out value='${oneVote.description}' /></td>
			<td style="border:1px solid;"><fmt:formatNumber type="number" 
            maxFractionDigits="3" value="${oneVote.votePercent}" /></td>
		</tr>
		<%
			i++;
		%>
	</c:forEach>
	</table>
	
	<table>
	<tr style="background-color: #333333;color: #FFFFFF;">
		<th style="border:1px solid;" colspan="2">Recent Votes in Delhi</th>
	</tr>
	<c:forEach items="${latestVotes}" var="oneVote">
		<tr>
			<td style="border:1px solid;"><img width="50" src="<c:out value='${oneVote.profilePic}' />" /></td>
			<td style="border:1px solid;"><c:out value='${oneVote.votingMessage}' /></td>
		</tr>
		<%
			i++;
		%>
	</c:forEach>
	</table>
	
	<table>
	<tr style="background-color: #333333;color: #FFFFFF;">
		<th style="border:1px solid;" colspan="2">Party Seats in Delhi</th>
	</tr>
	<tr style="background-color: #333333;color: #FFFFFF;">
		<th style="border:1px solid;" >Party Name</th>
		<th style="border:1px solid;" >Seats</th>
	</tr>
	<c:forEach items="${partySeats}" var="onePartySeat">
		<tr>
			<td style="border:1px solid;"><c:out value='${onePartySeat.partyName}' /></td>
			<td style="border:1px solid;"><c:out value='${onePartySeat.totalSeats}' /></td>
		</tr>
		<%
			i++;
		%>
	</c:forEach>
	</table>
</td>
</tr>
</table>

	
</body>
</html>
