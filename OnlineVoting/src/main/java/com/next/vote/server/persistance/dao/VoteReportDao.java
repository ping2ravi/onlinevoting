package com.next.vote.server.persistance.dao;

import java.util.List;

import com.next.vote.server.persistance.VoteReport;

public interface VoteReportDao {

	public abstract VoteReport saveVoteReport(VoteReport voteReport);

	public abstract List<VoteReport> getVoteReportsByType(String reportType);
	
	public abstract VoteReport getVoteReportByType(String reportType);
	
	public VoteReport getVoteReportByTypeAndPk(String reportType,Long primaryKey);

}