package com.next.vote.server.persistance.dao.impl;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.stereotype.Repository;

import com.next.vote.server.persistance.Election;
import com.next.vote.server.persistance.dao.ElectionDao;

@Repository
public class ElectionDaoHibernateSpringImpl extends BaseDaoHibernateSpring<Election> implements ElectionDao  {

	/* (non-Javadoc)
	 * @see com.next.aap.server.persistance.dao.impl.ElectionDao#saveElection(com.next.aap.server.persistance.Election)
	 */
	@Override
	public Election saveElection(Election election) 
	{
		checkIfStringMissing("Name", election.getName());
		checkIfElectionExistsWithSameName(election);
		election.setNameUp(election.getName().toUpperCase());
		election = super.saveObject(election);
		return election;
	}
	

	/* (non-Javadoc)
	 * @see com.next.aap.server.persistance.dao.impl.ElectionDao#deleteElection(com.next.aap.server.persistance.Election)
	 */
	@Override
	public void deleteElection(Election election)  {
		super.deleteObject(election);
	}

	/* (non-Javadoc)
	 * @see com.next.aap.server.persistance.dao.impl.ElectionDao#getElectionById(java.lang.Long)
	 */
	@Override
	public Election getElectionById(Long id) 
	{
		Election election = super.getObjectById(Election.class, id);
		return election;
	}

	/* (non-Javadoc)
	 * @see com.next.aap.server.persistance.dao.impl.ElectionDao#getAllElections()
	 */
	@Override
	public List<Election> getAllElections() 
	{
		List<Election> list = executeQueryGetList("from Election order by name asc");
		return list;
	}
	private void checkIfElectionExistsWithSameName(Election election) {
		Map<String, Object> params = new TreeMap<String, Object>();
		params.put("nameUp", election.getName());
		Election existingElection;
		if(election.getId() != null && election.getId() > 0){
			params.put("id", election.getId());
			existingElection = executeQueryGetObject("from Election where nameUp=:nameUp and id != :id", params);
		}else{
			existingElection = executeQueryGetObject("from Election where nameUp=:nameUp ", params);
		}
		
		if(existingElection != null){
			throw new RuntimeException("Election already exists with name = "+election.getName());
		}
	}
}