package com.next.vote.server.persistance.dao.impl;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.stereotype.Repository;

import com.next.vote.server.persistance.VoteReport;
import com.next.vote.server.persistance.dao.VoteReportDao;

@Repository
public class VoteReportDaoHibernateSpringImpl extends BaseDaoHibernateSpring<VoteReport> implements VoteReportDao  {

	/* (non-Javadoc)
	 * @see com.next.vote.server.persistance.dao.impl.VoteReportDao#saveVoteReport(com.next.vote.server.persistance.VoteReport)
	 */
	@Override
	public VoteReport saveVoteReport(VoteReport voteReport)
	{
		if(voteReport.getTotalCount() < 0){
			voteReport.setTotalCount(0);
		}
		voteReport = super.saveObject(voteReport);
		return voteReport;
	}

	/* (non-Javadoc)
	 * @see com.next.vote.server.persistance.dao.impl.VoteReportDao#getVoteReportOfByType(java.lang.String)
	 */
	@Override
	public List<VoteReport> getVoteReportsByType(String reportType){
		Map<String, Object> params = new TreeMap<String, Object>();
		params.put("reportType", reportType);
		List<VoteReport> list = executeQueryGetList("from VoteReport where reportType = :reportType",params);
		return list;
	}

	@Override
	public VoteReport getVoteReportByType(String reportType) {
		Map<String, Object> params = new TreeMap<String, Object>();
		params.put("reportType", reportType);
		VoteReport list = executeQueryGetObject("from VoteReport where reportType = :reportType",params);
		return list;
	}
	@Override
	public VoteReport getVoteReportByTypeAndPk(String reportType,Long primaryKey) {
		Map<String, Object> params = new TreeMap<String, Object>();
		params.put("reportType", reportType);
		params.put("primaryKey", primaryKey);
		VoteReport voteReport = executeQueryGetObject("from VoteReport where reportType = :reportType and primaryKey=:primaryKey",params);
		if(voteReport == null){
			//System.out.println("Report is null so creating one now");
			voteReport = new VoteReport();
			voteReport.setPrimaryKey(primaryKey);
			voteReport.setReportType(reportType);
			voteReport.setTotalCount(0);
			voteReport = this.saveVoteReport(voteReport);
		}
		//System.out.println("ReportType="+voteReport.getReportType()+", TotalCount="+voteReport.getTotalCount());
		return voteReport;
	}
}