package com.next.vote.server.persistance.dao.impl;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.stereotype.Repository;

import com.google.gdata.util.common.base.StringUtil;
import com.next.vote.server.persistance.Party;
import com.next.vote.server.persistance.dao.PartyDao;

@Repository
public class PartyDaoHibernateSpringImpl extends BaseDaoHibernateSpring<Party> implements PartyDao  {

	/* (non-Javadoc)
	 * @see com.next.aap.server.persistance.dao.impl.PartyDao#saveParty(com.next.aap.server.persistance.Party)
	 */
	@Override
	public Party saveParty(Party party) 
	{
		checkIfStringMissing("Name", party.getName());
		checkIfPartyExistsWithSameName(party);
		party.setNameUp(party.getName().toUpperCase());
		if(StringUtil.isEmpty(party.getUrlName())){
			party.setUrlName(getUrlName(party.getName()));	
		}
		party = super.saveObject(party);
		return party;
	}
	

	/* (non-Javadoc)
	 * @see com.next.aap.server.persistance.dao.impl.PartyDao#deleteParty(com.next.aap.server.persistance.Party)
	 */
	@Override
	public void deleteParty(Party party)  {
		super.deleteObject(party);
	}

	/* (non-Javadoc)
	 * @see com.next.aap.server.persistance.dao.impl.PartyDao#getPartyById(java.lang.Long)
	 */
	@Override
	public Party getPartyById(Long id) 
	{
		Party party = super.getObjectById(Party.class, id);
		return party;
	}

	/* (non-Javadoc)
	 * @see com.next.aap.server.persistance.dao.impl.PartyDao#getAllPartys()
	 */
	@Override
	public List<Party> getAllParties() 
	{
		List<Party> list = executeQueryGetList("from Party order by name asc");
		return list;
	}
	private void checkIfPartyExistsWithSameName(Party party) {
		Map<String, Object> params = new TreeMap<String, Object>();
		params.put("nameUp", party.getName());
		Party existingParty;
		if(party.getId() != null && party.getId() > 0){
			params.put("id", party.getId());
			existingParty = executeQueryGetObject("from Party where nameUp=:nameUp and id != :id", params);
		}else{
			existingParty = executeQueryGetObject("from Party where nameUp=:nameUp ", params);
		}
		
		if(existingParty != null){
			throw new RuntimeException("Party already exists with name = "+party.getName());
		}
	}


	@Override
	public Party getPartyByUrlName(String urlName) {
		Map<String, Object> params = new TreeMap<String, Object>();
		params.put("urlName", urlName.toLowerCase());
		Party existingParty = executeQueryGetObject("from Party where urlName = :urlName ", params);
		return existingParty;
	}


	@Override
	public Party getPartyByName(String name) {
		Map<String, Object> params = new TreeMap<String, Object>();
		params.put("nameUp", name.toUpperCase());
		Party existingParty = executeQueryGetObject("from Party where nameUp = :nameUp ", params);
		return existingParty;
	}
}