package com.next.vote.server.persistance.dao.impl;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.stereotype.Component;

import com.next.vote.server.persistance.Candidate;
import com.next.vote.server.persistance.dao.CandidateDao;

@Component
public class CandidateDaoHibernateSpringImpl extends BaseDaoHibernateSpring<Candidate> implements CandidateDao {

	/* (non-Javadoc)
	 * @see com.next.aap.server.persistance.dao.impl.CandidateDao#saveCandidate(com.next.aap.server.persistance.Candidate)
	 */
	@Override
	public Candidate saveCandidate(Candidate candidate)
	{
		checkIfStringMissing("Name", candidate.getName());
		checkIfObjectMissing("AssemblyConstituency", candidate.getAssemblyConstituency());
		checkIfCandidateExistsWithSameName(candidate);
		candidate.setNameUp(candidate.getName().toUpperCase());
		candidate = super.saveObject(candidate);
		return candidate;
	}

	/* (non-Javadoc)
	 * @see com.next.aap.server.persistance.dao.impl.CandidateDao#deleteCandidate(com.next.aap.server.persistance.Candidate)
	 */
	@Override
	public void deleteCandidate(Candidate candidate) {
		super.deleteObject(candidate);
	}

	/* (non-Javadoc)
	 * @see com.next.aap.server.persistance.dao.impl.CandidateDao#getCandidateById(java.lang.Long)
	 */
	@Override
	public Candidate getCandidateById(Long id)
	{
		Candidate candidate = super.getObjectById(Candidate.class, id);
		return candidate;
	}

	/* (non-Javadoc)
	 * @see com.next.aap.server.persistance.dao.impl.CandidateDao#getAllCandidates()
	 */
	@Override
	public List<Candidate> getAllCandidates()
	{
		List<Candidate> list = executeQueryGetList("from Candidate order by name asc");
		return list;
	}
	/* (non-Javadoc)
	 * @see com.next.aap.server.persistance.dao.impl.CandidateDao#getCandidateOfAssemblyConstituency(long)
	 */
	@Override
	public List<Candidate> getCandidateOfAssemblyConstituency(long electionId, long assemblyConstituencyId){
		Map<String, Object> params = new TreeMap<String, Object>();
		params.put("assemblyConstituencyId", assemblyConstituencyId);
		params.put("electionId", electionId);
		List<Candidate> list = executeQueryGetList("from Candidate where electionId = :electionId and assemblyConstituencyId = :assemblyConstituencyId order by party.name asc",params);
		return list;
	}

	@Override
	public List<Candidate> getCandidateOfAssemblyConstituency(long electionId, String urlNameOfassemblyConstituency){
		Map<String, Object> params = new TreeMap<String, Object>();
		params.put("urlname", urlNameOfassemblyConstituency.toLowerCase());
		params.put("electionId", electionId);
		List<Candidate> list = executeQueryGetList("from Candidate where electionId = :electionId and assemblyConstituency.urlName = :urlname order by party.name asc", params);
		return list;
	}
	private void checkIfCandidateExistsWithSameName(Candidate candidate) {
		if(candidate.getName().equals("Candidate Not Declared")){
			return;
		}
		Map<String, Object> params = new TreeMap<String, Object>();
		params.put("assemblyConstituencyId", candidate.getAssemblyConstituency().getId());
		params.put("nameUp", candidate.getName().toUpperCase());
		Candidate existingCandidate = null;
		if(candidate.getId() != null && candidate.getId() > 0){
			params.put("id", candidate.getId());
			existingCandidate = executeQueryGetObject("from Candidate where assemblyConstituencyId = :assemblyConstituencyId and nameUp = :nameUp and id != :id order by name asc",params);
		}else{
			existingCandidate = executeQueryGetObject("from Candidate where assemblyConstituencyId = :assemblyConstituencyId and nameUp = :nameUp order by name asc",params);
		}
		
		if(existingCandidate != null){
			throw new RuntimeException("Candidate already exists with name = "+candidate.getName()+" in Assembly Constituency "+candidate.getAssemblyConstituency().getName());
		}
	}

	@Override
	public List<Candidate> getAllCandidatesOfParty(long electionId, long partyId) {
		Map<String, Object> params = new TreeMap<String, Object>();
		params.put("partyId", partyId);
		params.put("electionId", electionId);
		List<Candidate> list = executeQueryGetList("from Candidate where electionId = :electionId and partyId = :partyId order by name asc",params);
		return list;
	}

	@Override
	public Candidate getPartyCandidateOfAssemblyConstituency(long electionId,
			long assemblyConstituencyId, long partyId) {
		Map<String, Object> params = new TreeMap<String, Object>();
		params.put("partyId", partyId);
		params.put("electionId", electionId);
		params.put("assemblyConstituencyId", assemblyConstituencyId);
		Candidate oneCandidate = executeQueryGetObject("from Candidate where electionId = :electionId and partyId = :partyId and assemblyConstituencyId=:assemblyConstituencyId",params);
		return oneCandidate;
	}
}