package com.next.vote.server.util;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.next.vote.server.service.VotingService;

public class CreateStateCandidateUtil {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("/spring/voting-core.xml");
		VotingService votingService = applicationContext.getBean(VotingService.class);
		System.out.println("voting Service "+votingService);
		votingService.createCandidatesForAStateElection(10L, 1L);
		applicationContext.close();
	}
	

}
