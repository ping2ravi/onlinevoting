package com.next.vote.server.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.primefaces.model.chart.PieChartModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.google.gdata.util.common.base.StringUtil;
import com.next.vote.client.dto.AssemblyConstituencyDto;
import com.next.vote.client.dto.CandidateDto;
import com.next.vote.client.dto.CandidateVoteDto;
import com.next.vote.client.dto.GenericVoteDto;
import com.next.vote.client.dto.StateDto;
import com.next.vote.client.dto.UserDto;
import com.next.vote.client.dto.UserVoteDto;
import com.next.vote.server.controller.jsf.BaseJsfBean;
import com.next.vote.server.service.Constants;
import com.next.vote.server.service.VoteReportService;
import com.next.vote.server.service.VotingService;
import com.next.vote.web.util.VotingSessionUtil;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLActions;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

@Controller
public class AssemblyConstituencyVotingController extends BaseController {

	private static final long serialVersionUID = 1L;


	
	@Autowired
	private VoteReportService voteReportService;
	
	@Autowired
	private VotingService votingService;

	@RequestMapping(value="/voting/{version}/delhi/{acName}", method = RequestMethod.GET)
	public ModelAndView showVotingScreenForVersion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, 
			ModelAndView mv,@PathVariable String version,  @PathVariable String acName) throws Exception {
		UserDto loggedInuser = VotingSessionUtil.getUserFromSession(httpServletRequest);
		if(loggedInuser == null){
			String redirectUrl = BaseJsfBean.buildLoginUrl(httpServletRequest, "/myconstituency");
			redirectToViewAfterLogin(redirectUrl, mv);
		}else{
			
		}
		mv.setViewName(version+"/voting");
		return mv;
		
	}
	
	@RequestMapping(value="/ac/delhi/{acName}", method = RequestMethod.GET)
	public ModelAndView showVotingScreen(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, 
			ModelAndView mv, @PathVariable String acName) throws Exception {
		return showVotingScreenForVersion(httpServletRequest, httpServletResponse, mv, defaultVersion, acName);
		
	}
	
	private void refreshData(String acName,ModelAndView mv){
		List<CandidateDto> candidates;
		CandidateDto selectedCandidate;
		List<CandidateVoteDto> candidateVotes;
		List<GenericVoteDto> genderVotes;
		List<GenericVoteDto> privatePublicVotes;
		List<GenericVoteDto> ageVotes;
		List<UserVoteDto> userVotes;
		AssemblyConstituencyDto assemblyConstituency;

		candidates = votingService.getCandidatesOfAssmbleConstituency(Constants.DELHI_ELECTION_ID, acName);
    	for(CandidateDto oneCandidate:candidates){
    		if(StringUtil.isEmpty(oneCandidate.getProfilePic())){
    			oneCandidate.setProfilePic("https://cdn1.iconfinder.com/data/icons/free-large-boss-icon-set/128/Caucasian_boss.png");
    		}
    	}
    	mv.getModel().put("candidate", candidates);
    	long acId = candidates.get(0).getAssemblyConstituencyId();
    	assemblyConstituency = votingService.getAssemblyConstituencyById(acId);
    	mv.getModel().put("assemblyConstituency", assemblyConstituency);
    	/*
    	candidateVotes = voteReportService.getCandidateVotesOfAssemblyConstituency(acId);
    	genderVotes = voteReportService.getGenderVoteInAssemblyConstituency(acId);
    	privatePublicVotes = voteReportService.getPublicPrivateVoteInAssemblyConstituency(acId);
    	ageVotes = voteReportService.getAgeVoteInAssemblyConstituency(acId);
    	userVotes = votingService.getLatestVoteForAssemblyConttituencyAndElection(acId, Constants.DELHI_ELECTION_ID);
         */
	}


}
