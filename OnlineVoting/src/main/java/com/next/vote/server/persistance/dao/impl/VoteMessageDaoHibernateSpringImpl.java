package com.next.vote.server.persistance.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.next.vote.server.persistance.VoteMessage;
import com.next.vote.server.persistance.dao.VoteMessageDao;

@Repository
public class VoteMessageDaoHibernateSpringImpl extends BaseDaoHibernateSpring<VoteMessage> implements VoteMessageDao  {

	private static final long serialVersionUID = 1L;

	/* (non-Javadoc)
	 * @see com.next.vote.server.persistance.dao.impl.VoreDao#saveVoteMessage(com.next.vote.server.persistance.VoteMessage)
	 */
	@Override
	public VoteMessage saveVoteMessage(VoteMessage voteMessage) 
	{
		checkIfObjectMissing("Assembly Constituency", voteMessage.getAssemblyConstituency());
		checkIfObjectMissing("Candidate", voteMessage.getCandidate());
		checkIfObjectMissing("Party", voteMessage.getParty());
		checkIfObjectMissing("User", voteMessage.getUser());
		voteMessage = super.saveObject(voteMessage);
		return voteMessage;
	}
	

	/* (non-Javadoc)
	 * @see com.next.vote.server.persistance.dao.impl.VoreDao#deleteVoteMessage(com.next.vote.server.persistance.VoteMessage)
	 */
	@Override
	public void deleteVoteMessage(VoteMessage voteMessage)  {
		super.deleteObject(voteMessage);
	}

	/* (non-Javadoc)
	 * @see com.next.vote.server.persistance.dao.impl.VoreDao#getVoteMessageById(java.lang.Long)
	 */
	@Override
	public VoteMessage getVoteMessageById(Long id) 
	{
		VoteMessage voteMessage = super.getObjectById(VoteMessage.class, id);
		return voteMessage;
	}

	/* (non-Javadoc)
	 * @see com.next.vote.server.persistance.dao.impl.VoreDao#getAllVoteMessages()
	 */
	@Override
	public List<VoteMessage> getAllVoteMessages() 
	{
		List<VoteMessage> list = executeQueryGetList("from VoteMessage");
		return list;
	}
	
	/* (non-Javadoc)
	 * @see com.next.vote.server.persistance.dao.impl.VoreDao#getAllVoteMessagesByAc(java.lang.Long)
	 */
	@Override
	public List<VoteMessage> getAllVoteMessagesByAc(Long assemblyConstituencyId) 
	{
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("assemblyConstituencyId", assemblyConstituencyId);
		List<VoteMessage> list = executeQueryGetList("from VoteMessage where assemblyConstituencyId = :assemblyConstituencyId", params);
		return list;
	}

	/* (non-Javadoc)
	 * @see com.next.vote.server.persistance.dao.impl.VoreDao#getAllVoteMessagesByCandidate(java.lang.Long)
	 */
	@Override
	public List<VoteMessage> getAllVoteMessagesByCandidate(Long candidateId) 
	{
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("candidateId", candidateId);
		List<VoteMessage> list = executeQueryGetList("from VoteMessage where candidateId = :candidateId", params);
		return list;
	}

	/* (non-Javadoc)
	 * @see com.next.vote.server.persistance.dao.impl.VoreDao#getAllVoteMessagesByParty(java.lang.Long)
	 */
	@Override
	public List<VoteMessage> getAllVoteMessagesByParty(Long partyId) 
	{
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("partyId", partyId);
		List<VoteMessage> list = executeQueryGetList("from VoteMessage where partyId = :partyId", params);
		return list;
	}


	@Override
	public VoteMessage getVoteMessagesByUserForElection(Long userId, Long electionId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("userId", userId);
		params.put("electionId", electionId);
		VoteMessage voteMessage = executeQueryGetObject("from VoteMessage where userId = :userId and electionId=:electionId", params);
		return voteMessage;
	}


	@Override
	public List<VoteMessage> getAllVoteMessagesByElection(Long electionId, int pageSize) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("electionId", electionId);
		List<VoteMessage> list = executeQueryGetList("from VoteMessage where electionId = :electionId order by votingTime desc", params, pageSize);
		return list;
	}


	@Override
	public List<VoteMessage> getAllVoteMessagesByAcAndElection(Long assemblyConstituencyId,Long electionId,  int pageSize) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("assemblyConstituencyId", assemblyConstituencyId);
		params.put("electionId", electionId);
		List<VoteMessage> list = executeQueryGetList("from VoteMessage where assemblyConstituencyId = :assemblyConstituencyId and electionId = :electionId order by votingTime desc", params, pageSize);
		return list;
	}
	
	@Override
	public List<VoteMessage> getAllVoteMessagesByDistrictAndElection(Long districtId,Long electionId,  int pageSize) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("districtId", districtId);
		params.put("electionId", electionId);
		List<VoteMessage> list = executeQueryGetList("from VoteMessage where districtId = :districtId and electionId = :electionId order by votingTime desc", params, pageSize);
		return list;
	}
	
	@Override
	public List<VoteMessage> getAllVoteMessagesByStateAndElection(Long stateId,Long electionId,  int pageSize) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("stateId", stateId);
		params.put("electionId", electionId);
		List<VoteMessage> list = executeQueryGetList("from VoteMessage where stateId = :stateId and electionId = :electionId order by votingTime desc", params, pageSize);
		return list;
	}

}