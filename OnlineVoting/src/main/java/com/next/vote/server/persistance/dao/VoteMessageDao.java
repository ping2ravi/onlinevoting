package com.next.vote.server.persistance.dao;

import java.util.List;

import com.next.vote.server.persistance.VoteMessage;

public interface VoteMessageDao {

	public abstract VoteMessage saveVoteMessage(VoteMessage state);

	public abstract void deleteVoteMessage(VoteMessage state);

	public abstract VoteMessage getVoteMessageById(Long id);

	public abstract List<VoteMessage> getAllVoteMessages();

	public abstract List<VoteMessage> getAllVoteMessagesByAc(Long assemblyConstituencyId);
	
	public abstract List<VoteMessage> getAllVoteMessagesByAcAndElection(Long assemblyConstituencyId,Long electionId, int pageSize);
	
	public abstract List<VoteMessage> getAllVoteMessagesByElection(Long electionId, int pageSize);

	public abstract List<VoteMessage> getAllVoteMessagesByCandidate(Long candidateId);

	public abstract List<VoteMessage> getAllVoteMessagesByParty(Long partyId);
	
	public abstract VoteMessage getVoteMessagesByUserForElection(Long userId, Long electionId);
	
	public abstract List<VoteMessage> getAllVoteMessagesByDistrictAndElection(Long districtId,Long electionId,  int pageSize);
	
	public abstract List<VoteMessage> getAllVoteMessagesByStateAndElection(Long stateId,Long electionId,  int pageSize);
	

}