package com.next.vote.server.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.auth.AccessToken;

import com.next.vote.client.dto.AssemblyConstituencyDto;
import com.next.vote.client.dto.CandidateDto;
import com.next.vote.client.dto.DistrictDto;
import com.next.vote.client.dto.ElectionDto;
import com.next.vote.client.dto.PollingStationDto;
import com.next.vote.client.dto.StateDto;
import com.next.vote.client.dto.UserDto;
import com.next.vote.client.dto.UserVoteDto;
import com.next.vote.client.exception.VotingException;
import com.next.vote.server.persistance.AssemblyConstituency;
import com.next.vote.server.persistance.Candidate;
import com.next.vote.server.persistance.District;
import com.next.vote.server.persistance.Election;
import com.next.vote.server.persistance.FacebookAccount;
import com.next.vote.server.persistance.Party;
import com.next.vote.server.persistance.PollingStation;
import com.next.vote.server.persistance.State;
import com.next.vote.server.persistance.TwitterAccount;
import com.next.vote.server.persistance.User;
import com.next.vote.server.persistance.Vote;
import com.next.vote.server.persistance.VoteMessage;
import com.next.vote.server.persistance.VoteReport;
import com.next.vote.server.persistance.dao.AssemblyConstituencyDao;
import com.next.vote.server.persistance.dao.CandidateDao;
import com.next.vote.server.persistance.dao.DistrictDao;
import com.next.vote.server.persistance.dao.ElectionDao;
import com.next.vote.server.persistance.dao.FacebookAccountDao;
import com.next.vote.server.persistance.dao.PartyDao;
import com.next.vote.server.persistance.dao.PollingStationDao;
import com.next.vote.server.persistance.dao.StateDao;
import com.next.vote.server.persistance.dao.TwitterAccountDao;
import com.next.vote.server.persistance.dao.UserDao;
import com.next.vote.server.persistance.dao.VoteDao;
import com.next.vote.server.persistance.dao.VoteMessageDao;
import com.next.vote.server.persistance.dao.VoteReportDao;

@Service("votingService")
public class VotingServiceImpl implements VotingService,Serializable{

	
	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private UserDao userDao;
	@Autowired
	private FacebookAccountDao facebookAccountDao;
	@Autowired
	private StateDao stateDao;
	@Autowired
	private DistrictDao districtDao;
	@Autowired
	private AssemblyConstituencyDao assemblyConstituencyDao;
	@Autowired
	private CandidateDao candidateDao;
	@Autowired
	private TwitterAccountDao twitterAccountDao;
	@Autowired
	private VoteDao voteDao;
	@Autowired
	private ElectionDao electionDao;
	@Autowired
	private PollingStationDao pollingStationDao;
	@Autowired
	private PartyDao partyDao;
	@Autowired
	private VoteReportDao voteReportDao;
	@Autowired
	private VoteMessageDao voteMessageDao;

	@Override
	@Transactional
	public UserDto saveUser(UserDto userDto) {
		User user;
		if(userDto.getId() == null || userDto.getId() == 0){
			user = new User();
		}else{
			user = userDao.getUserById(userDto.getId());
		}
		
		if(userDto.getAssemblyConstituencyLivingId() != null && userDto.getAssemblyConstituencyLivingId() != 0){
			AssemblyConstituency assemblyConstituencyLiving = assemblyConstituencyDao.getAssemblyConstituencyById(userDto.getAssemblyConstituencyLivingId());
			user.setAssemblyConstituencyLiving(assemblyConstituencyLiving);
			user.setAssemblyConstituencyLivingId(assemblyConstituencyLiving.getId());
			user.setDistrictLiving(assemblyConstituencyLiving.getDistrict());
			user.setDistrictLivingId(assemblyConstituencyLiving.getDistrict().getId());
			user.setStateLiving(assemblyConstituencyLiving.getDistrict().getState());
			user.setStateLivingId(assemblyConstituencyLiving.getDistrict().getStateId());
		}else{
			if(userDto.getDistrictLivingId() != null && userDto.getDistrictLivingId() != 0){
				District districtLiving = districtDao.getDistrictById(userDto.getDistrictLivingId());
				user.setDistrictLiving(districtLiving);
				user.setDistrictLivingId(districtLiving.getId());
				user.setStateLiving(districtLiving.getState());
				user.setStateLivingId(districtLiving.getStateId());
			}else{
				if(userDto.getStateLivingId() != null && userDto.getStateLivingId() != 0){
					State stateLiving = stateDao.getStateById(userDto.getStateLivingId());
					user.setStateLiving(stateLiving);
					user.setStateLivingId(stateLiving.getId());
				}
			}
		}
		if(userDto.getAssemblyConstituencyVotingId() != null && userDto.getAssemblyConstituencyVotingId() != 0){
			AssemblyConstituency assemblyConstituencyVoting = assemblyConstituencyDao.getAssemblyConstituencyById(userDto.getAssemblyConstituencyVotingId());
			user.setAssemblyConstituencyVoting(assemblyConstituencyVoting);
			user.setAssemblyConstituencyVotingId(assemblyConstituencyVoting.getId());
			user.setDistrictVoting(assemblyConstituencyVoting.getDistrict());
			user.setDistrictVotingId(assemblyConstituencyVoting.getDistrict().getId());
			user.setStateVoting(assemblyConstituencyVoting.getDistrict().getState());
			user.setStateVotingId(assemblyConstituencyVoting.getDistrict().getStateId());
		}else{
			if(userDto.getDistrictVotingId() != null && userDto.getDistrictVotingId() != 0){
				District districtVoting = districtDao.getDistrictById(userDto.getDistrictVotingId());
				user.setDistrictVoting(districtVoting);
				user.setDistrictVotingId(districtVoting.getId());
				user.setStateVoting(districtVoting.getState());
				user.setStateVotingId(districtVoting.getStateId());
			}else{
				if(userDto.getStateVotingId() != null && userDto.getStateVotingId() != 0){
					State stateVoting = stateDao.getStateById(userDto.getStateVotingId());
					user.setStateVoting(stateVoting);
					user.setStateVotingId(stateVoting.getId());
				}
			}
			
		}
		
		user.setMobile(userDto.getMobile());
		user.setName(userDto.getName());
		user.setDateOfBirth(userDto.getDateOfBirth());
		user.setGender(userDto.getGender());
		user = userDao.saveUser(user);
		return convertUser(user);
	}
	
	UserDto convertUser(User user){
		UserDto userDto = new UserDto();
		BeanUtils.copyProperties(user, userDto);
		if(user.getAssemblyConstituencyVoting() != null){
			userDto.setAssemblyConstituencyVotingUrlName(user.getAssemblyConstituencyVoting().getUrlName());	
		}
		return userDto;
	}

	@Override
	@Transactional
	public User getUserById(Long id) {
		User user = userDao.getUserById(id);
		System.out.println(user.getId());
		return user;
	}

	@Override
	@Transactional
	public List<StateDto> getAllStates() {
		List<State> allStates = stateDao.getAllStates();
		List<StateDto> returnList = new ArrayList<StateDto>();
		for(State oneState:allStates){
			returnList.add(convertState(oneState));
		}
		return returnList;	
	}
	private StateDto convertState(State oneState){
		if(oneState == null){
			return null;
		}
		StateDto oneStateDto = new StateDto();
		oneStateDto.setId(oneState.getId());
		oneStateDto.setName(oneState.getName());
		oneStateDto.setDistrictDataAvailable(oneState.getDistrictDataAvailable());
		return oneStateDto;
	}

	@Override
	@Transactional
	public List<DistrictDto> getAllDistrictOfState(long stateId) {
		List<District> allDistricts = districtDao.getDistrictOfState(stateId);
		List<DistrictDto> returnList = new ArrayList<DistrictDto>();
		for(District oneDistrict:allDistricts){
			returnList.add(convertDistrict(oneDistrict));
		}
		return returnList;
	}
	private DistrictDto convertDistrict(District oneDistrict){
		DistrictDto oneDistrictDto = new DistrictDto();
		oneDistrictDto.setId(oneDistrict.getId());
		oneDistrictDto.setName(oneDistrict.getName());
		oneDistrictDto.setAcDataAvailable(oneDistrict.getAcDataAvailable());
		return oneDistrictDto;
	}

	@Override
	@Transactional
	public List<AssemblyConstituencyDto> getAllAssemblyConstituenciesOfDistrict(
			long districtId) {
		List<AssemblyConstituency> allAssemblyConstituencies = assemblyConstituencyDao.getAssemblyConstituencyOfDistrict(districtId);
		return convertAssemblyConstituencies(allAssemblyConstituencies);
	}
	private AssemblyConstituencyDto convertAssemblyConstituency(AssemblyConstituency oneAssemblyConstituency){
		if(oneAssemblyConstituency == null){
			return null;
		}
		AssemblyConstituencyDto oneAssemblyConstituencyDto = new AssemblyConstituencyDto();
		BeanUtils.copyProperties(oneAssemblyConstituency, oneAssemblyConstituencyDto);
		return oneAssemblyConstituencyDto;
	}
	private List<AssemblyConstituencyDto> convertAssemblyConstituencies(List<AssemblyConstituency> allAssemblyConstituencies){
		List<AssemblyConstituencyDto> returnList = new ArrayList<AssemblyConstituencyDto>(allAssemblyConstituencies.size());
		for(AssemblyConstituency oneAssemblyConstituency:allAssemblyConstituencies){
			returnList.add(convertAssemblyConstituency(oneAssemblyConstituency));
		}
		return returnList;
	}

	@Override
	@Transactional
	public StateDto saveState(StateDto stateDto) {
		State dbState;
		if(stateDto.getId() == null || stateDto.getId() <= 0){
			dbState = stateDao.getStateByName(stateDto.getName());
			if(dbState == null){
				dbState = new State();	
			}
		}else{
			dbState = stateDao.getStateById(stateDto.getId());
			if(dbState == null){
				throw new VotingException("No such states exist[id="+stateDto.getId()+"]");
			}
		}
		
		dbState.setName(stateDto.getName());
		dbState = stateDao.saveState(dbState);
		
		stateDto.setId(dbState.getId());
		return stateDto;
	}

	@Override
	@Transactional
	public DistrictDto saveDistrict(DistrictDto districtWeb) {
		District dbDistrict;
		if(districtWeb.getId() == null || districtWeb.getId() <= 0){
			dbDistrict = districtDao.getDistrictByNameAndStateId(districtWeb.getStateId(), districtWeb.getName());
			if(dbDistrict == null){
				dbDistrict = new District();	
			}
		}else{
			dbDistrict = districtDao.getDistrictById(districtWeb.getId());
			if(dbDistrict == null){
				throw new VotingException("No such District exist[id="+districtWeb.getId()+"]");
			}
		}
		dbDistrict.setName(districtWeb.getName());
		
		State state = stateDao.getStateById(districtWeb.getStateId());
		dbDistrict.setState(state);
		
		
		dbDistrict = districtDao.saveDistrict(dbDistrict);
		
		districtWeb.setId(dbDistrict.getId());
		return districtWeb;
	}

	@Override
	@Transactional
	public List<DistrictDto> saveDistricts(List<DistrictDto> districtList) {
		List<DistrictDto> returnList = new ArrayList<DistrictDto>();
		for(DistrictDto oneDistrictDto:districtList){
			returnList.add(saveDistrict(oneDistrictDto));
		}
		return returnList;
	}

	@Override
	@Transactional
	public AssemblyConstituencyDto saveAssemblyConstituency(
			AssemblyConstituencyDto assemblyConstituencyWeb) {
		AssemblyConstituency dbAssemblyConstituency;
		if(assemblyConstituencyWeb.getId() == null || assemblyConstituencyWeb.getId() <= 0){
			dbAssemblyConstituency = assemblyConstituencyDao.getAssemblyConstituencyNameAndDistrictId(assemblyConstituencyWeb.getDistrictId(), assemblyConstituencyWeb.getName());
			if(dbAssemblyConstituency == null){
				dbAssemblyConstituency = new AssemblyConstituency();	
			}
		}else{
			dbAssemblyConstituency = assemblyConstituencyDao.getAssemblyConstituencyById(assemblyConstituencyWeb.getId());
			if(dbAssemblyConstituency == null){
				throw new VotingException("No such Assembly Constituency exist[id="+assemblyConstituencyWeb.getId()+"]");
			}
		}
		dbAssemblyConstituency.setName(assemblyConstituencyWeb.getName());
		District district = districtDao.getDistrictById(assemblyConstituencyWeb.getDistrictId());
		dbAssemblyConstituency.setDistrict(district);
		
		dbAssemblyConstituency = assemblyConstituencyDao.saveAssemblyConstituency(dbAssemblyConstituency);
		
		assemblyConstituencyWeb.setId(dbAssemblyConstituency.getId());
		return assemblyConstituencyWeb;
	}

	@Override
	@Transactional
	public List<AssemblyConstituencyDto> saveAssemblyConstituencies(
			List<AssemblyConstituencyDto> assemblyConstituencyList) {
		List<AssemblyConstituencyDto> returnList = new ArrayList<AssemblyConstituencyDto>();
		for(AssemblyConstituencyDto oneAssemblyConstituencyDto:assemblyConstituencyList){
			returnList.add(saveAssemblyConstituency(oneAssemblyConstituencyDto));
		}
		return returnList;
	}

	@Override
	@Transactional
	public CandidateDto saveCandidate(CandidateDto candidateDto) {
		Candidate dbCandidate = null;
		if(candidateDto.getId() != null && candidateDto.getId() > 0){
			dbCandidate = candidateDao.getCandidateById(candidateDto.getId());
			if(dbCandidate == null){
				throw new VotingException("No candidate found with id "+candidateDto.getId());
			}
		}else{
			dbCandidate = new Candidate();			
		}
		dbCandidate.setAddress(candidateDto.getAddress());
		dbCandidate.setContactNumber1(candidateDto.getContactNumber1());
		dbCandidate.setContactNumber2(candidateDto.getContactNumber2());
		dbCandidate.setEducation(candidateDto.getEducation());
		dbCandidate.setLegalCases(candidateDto.getLegalCases());
		dbCandidate.setName(candidateDto.getName());
		dbCandidate.setObjectives(candidateDto.getObjectives());
		dbCandidate.setProfile(candidateDto.getProfile());
		dbCandidate.setProfilePic(candidateDto.getProfilePic());
		dbCandidate.setSourceOfIncome(candidateDto.getSourceOfIncome());
		dbCandidate.setWealth(candidateDto.getWealth());
		dbCandidate.setAssemblyConstituencyId(candidateDto.getAssemblyConstituencyId());
		
		AssemblyConstituency dbAssemblyConstituency = assemblyConstituencyDao.getAssemblyConstituencyById(candidateDto.getAssemblyConstituencyId());
		dbCandidate.setAssemblyConstituency(dbAssemblyConstituency);
		
		dbCandidate = candidateDao.saveCandidate(dbCandidate);
		
		return convertCandidate(dbCandidate);
	}

	@Override
	@Transactional
	public List<CandidateDto> getAllCandidates() {
		List<Candidate> candidates = candidateDao.getAllCandidates();
		List<CandidateDto> allCandidate = new ArrayList<CandidateDto>();
		for(Candidate oneCandidate:candidates){
			allCandidate.add(convertCandidate(oneCandidate));
		}
		return allCandidate;
	}

	@Override
	@Transactional
	public UserDto saveFacebookAccount(com.restfb.types.User facebookUser,String token,Date tokenValidTill) {
		logger.info("saving Facebook account [?]",facebookUser.getUsername());
		FacebookAccount facebookAccount = facebookAccountDao.getFacebookAccountByFacebookUserId(facebookUser.getId());
		if(facebookAccount == null){
			facebookAccount = new FacebookAccount();
		}
		facebookAccount.setEmail(facebookUser.getEmail());
		facebookAccount.setFacebookUserId(facebookUser.getId());
		facebookAccount.setToken(token);
		facebookAccount.setTokenValidTill(tokenValidTill);
		if(facebookUser.getUsername() == null){
			facebookAccount.setUserName(facebookUser.getEmail());
		}else{
			facebookAccount.setUserName(facebookUser.getUsername());	
		}
		
		facebookAccount = facebookAccountDao.saveFacebookAccount(facebookAccount);

		User user = facebookAccount.getUser();
		if(user == null){
			//Create a new Blank User
			user = new User();
			user.setName(facebookUser.getName());
			user.setFbdateOfBirth(facebookUser.getBirthdayAsDate());
			user = userDao.saveUser(user);
		}
		user.setProfilePic("http://graph.facebook.com/"+facebookAccount.getFacebookUserId()+"/picture?type=large");
		
		facebookAccount.setUser(user);
		
		return convertUser(user);
	}

	@Override
	@Transactional
	public UserDto saveTwitterAccount(Twitter twitter, AccessToken accessToken) {
		logger.info("saving Twitter account [?]",accessToken.getScreenName());
		TwitterAccount twitterAccount = twitterAccountDao.getTwitterAccountByTwitterId(accessToken.getUserId());
		if(twitterAccount == null){
			twitterAccount = new TwitterAccount();
		}
		twitterAccount.setScreenName(accessToken.getScreenName());
		twitterAccount.setToken(accessToken.getToken());
		twitterAccount.setTokenSecret(accessToken.getTokenSecret());
		twitterAccount.setTwitterId(accessToken.getUserId());
		twitterAccount = twitterAccountDao.saveTwitterAccount(twitterAccount);
		
		User user = twitterAccount.getUser();
		if(user == null){
			user = new User();
			user.setName(accessToken.getScreenName());
			user = userDao.saveUser(user);
		}
		try {
			twitter4j.User twitterUser = twitter.showUser(twitter.getId());
			user.setProfilePic(twitterUser.getOriginalProfileImageURL());
			System.out.println("twitterUser.getOriginalProfileImageURL()="+twitterUser.getOriginalProfileImageURL());
			System.out.println("twitterUser.getBiggerProfileImageURL()="+twitterUser.getBiggerProfileImageURL());
			System.out.println("twitterUser.getMiniProfileImageURL()="+twitterUser.getMiniProfileImageURL());
			System.out.println("twitterUser.getProfileImageURL()="+twitterUser.getProfileImageURL());
		} catch (IllegalStateException | TwitterException e) {
			e.printStackTrace();
		}
		twitterAccount.setUser(user);
		return convertUser(user);
	}

	@Override
	@Transactional
	public synchronized String saveVote(Long userId, Long candidateId, boolean publicVote) {
		VoteMessage voteMessage = new VoteMessage();
		
		StringBuilder sb = new StringBuilder("");
		Candidate candidate = candidateDao.getCandidateById(candidateId);
		if(candidate == null){
			throw new VotingException("No such candidate exists");
		}
		Vote existingVote = voteDao.getVotesByUserForElection(userId,candidate.getElectionId());
		Vote newVote;
		boolean voteChanged = false;
		if(existingVote == null){
			newVote = new Vote();
			newVote.setDateCreated(new Date());
			newVote.setDateModified(new Date());
			sb.append("your votes saved succesfully");
		}else{
			if(existingVote.getCandidateId().equals(candidateId)){
				logger.info("No change in vote");
				return "No change in vote.";
			}
			updateVotingReport(existingVote, -1);
			sb.append("Your vote succesfully changed from "+existingVote.getCandidate().getName()+"("+existingVote.getParty().getName() +") of "+existingVote.getAssemblyConstituency().getName()+" to ");
			newVote = existingVote;
			newVote.setDateModified(new Date());
			voteChanged = true;
		}
		newVote.setPublicVote(publicVote);
		
		User user = userDao.getUserById(userId);
		
		
		logger.info("saving vote for user[?] to candidate [?]",user.getName(),candidate.getName());
		
		newVote.setCandidate(candidate);
		newVote.setAssemblyConstituency(candidate.getAssemblyConstituency());
		newVote.setParty(candidate.getParty());
		newVote.setElection(candidate.getElection());
		newVote.setUser(user);

		newVote = voteDao.saveVote(newVote);
		updateVotingReport(newVote, 1);
		
		voteMessage.setAssemblyConstituency(newVote.getAssemblyConstituency());
		voteMessage.setAssemblyConstituencyName(newVote.getAssemblyConstituency().getName());
		voteMessage.setCandidate(newVote.getCandidate());
		voteMessage.setCandidateName(newVote.getCandidate().getName());
		voteMessage.setDateCreated(new Date());
		voteMessage.setDateModified(new Date());
		voteMessage.setDistrict(newVote.getAssemblyConstituency().getDistrict());
		voteMessage.setDistrictName(newVote.getAssemblyConstituency().getDistrict().getName());
		voteMessage.setElection(newVote.getElection());
		voteMessage.setParty(newVote.getParty());
		voteMessage.setPartyName(newVote.getParty().getName());
		voteMessage.setProfilePic(newVote.getUser().getProfilePic());
		voteMessage.setPublicVote(newVote.isPublicVote());
		voteMessage.setState(newVote.getAssemblyConstituency().getDistrict().getState());
		voteMessage.setStateName(newVote.getAssemblyConstituency().getDistrict().getState().getName());
		voteMessage.setUser(newVote.getUser());
		voteMessage.setUserName(newVote.getUser().getName());
		voteMessage.setVoteChange(voteChanged);
		if(newVote.isPublicVote()){
			if(newVote.getCandidate().isInfoAvailable()){
				voteMessage.setVotingMessage(newVote.getUser().getName() + " has voted for "+ newVote.getCandidate().getName()+" of "+newVote.getParty().getName() +" in Assembly constituency "+newVote.getAssemblyConstituency().getName());	
			}else{
				voteMessage.setVotingMessage(newVote.getUser().getName() + " has voted for "+newVote.getParty().getName()+" in Assembly constituency "+newVote.getAssemblyConstituency().getName());
			}
		}else{
			voteMessage.setVotingMessage(newVote.getUser().getName() + " has voted in Assembly constituency "+newVote.getAssemblyConstituency().getName());
		}

		voteMessage.setVotingTime(new Date());
		voteMessage = voteMessageDao.saveVoteMessage(voteMessage);
		if(voteChanged){
			sb.append(" "+newVote.getCandidate().getName()+"("+newVote.getParty().getName() +") of "+newVote.getAssemblyConstituency().getName());
		}
		return sb.toString();
	}
	
	private  void updateVotingReport(Vote vote, int voteCount){
		//Total Vote for elections
		updateElectionVotes(vote,voteCount);
		//Total Party Votes
		updatePartyVotes(vote, voteCount);
		//Candidate Votes
		updateCandidateVotes(vote, voteCount);
		
		//assembly Constituency Votes
		updateAssemblyConstituencyVotes(vote, voteCount);
		
	}
	private void updateElectionVotes(Vote vote, int voteCount){
		updateTotalVoteForElection(vote.getElection().getId(), voteCount);
		//Total Public Vote and Total Private vote
		if(vote.isPublicVote()){
			updateTotalVoteForElectionPublicVote(vote.getElection().getId(), voteCount);
		}else{
			updateTotalVoteForElectionPrivateVote(vote.getElection().getId(), voteCount);
		}
		updateTotalVoteForGenderVote(vote.getElection().getId(), vote.getUser().getGender(), voteCount);
		updateTotalVoteForAgeVote(vote.getElection().getId(), vote.getUser().getDateOfBirth(), vote.getElection().getElectionDate(), voteCount);
		
	}
	private void updatePartyVotes(Vote vote, int voteCount){
		updateTotalVoteForElectionPartyVote(vote.getParty().getId(), voteCount);
		//Total Public Vote and Total Private vote
		if(vote.isPublicVote()){
			updateTotalVoteForElectionPartyPublicVote(vote.getParty().getId(), voteCount);
		}else{
			updateTotalVoteForElectionPartyPrivateVote(vote.getParty().getId(), voteCount);
		}
		updateTotalVoteForGenderVoteForParty(vote.getElectionId(), vote.getUser().getGender(), voteCount);
	}
	private void updateCandidateVotes(Vote vote, int voteCount){
		updateTotalVoteForCandidate(vote.getCandidate().getId(), voteCount);
		//Total Public Vote and Total Private vote
		if(vote.isPublicVote()){
			updateTotalVoteForCandidatePublicVote(vote.getCandidate().getId(), voteCount);
		}else{
			updateTotalVoteForCandidatePrivateVote(vote.getCandidate().getId(), voteCount);
		}
	}
	private void updateAssemblyConstituencyVotes(Vote vote, int voteCount){
		updateTotalVoteForAc(vote.getAssemblyConstituency().getId(), voteCount);
		//Total Public Vote and Total Private vote
		if(vote.isPublicVote()){
			updateTotalVoteForAcPublicVote(vote.getAssemblyConstituency().getId(), voteCount);
		}else{
			updateTotalVoteForAcPrivateVote(vote.getAssemblyConstituency().getId(), voteCount);
		}
		updateTotalVoteForGenderVoteForAssemblyConstituency(vote.getAssemblyConstituency().getId(), vote.getUser().getGender(), voteCount);
		updateTotalVoteForAgeVoteForAssemblyConstituency(vote.getAssemblyConstituency().getId(), vote.getUser().getDateOfBirth(), vote.getElection().getElectionDate(), voteCount);
	}
	private void updateTotalVoteForElection(Long electionId, int voteCount){
		updateTotalVoteForVoteReportType(VoteReportTypes.TOTAL_ELECTION_VOTE, electionId, voteCount);
	}
	private void updateTotalVoteForElectionPublicVote(Long electionId, int voteCount){
		updateTotalVoteForVoteReportType(VoteReportTypes.TOTAL_ELECTION_PUBLIC_VOTE, electionId, voteCount);
	}
	private void updateTotalVoteForGenderVote(Long electionId,String gender, int voteCount){
		if(gender != null){
			if(gender.equalsIgnoreCase("MALE")){
				updateTotalVoteForVoteReportType(VoteReportTypes.TOTAL_ELECTION_MALE_VOTE, electionId, voteCount);
				return;
			}
			if(gender.equalsIgnoreCase("FEMALE")){
				updateTotalVoteForVoteReportType(VoteReportTypes.TOTAL_ELECTION_FEMALE_VOTE, electionId, voteCount);
				return;
			}
		}
		updateTotalVoteForVoteReportType(VoteReportTypes.TOTAL_ELECTION_NOT_SPECIFIED_VOTE, electionId, voteCount);	
	}
	private void updateTotalVoteForGenderVoteForParty(Long electionId,String gender, int voteCount){
		if(gender != null){
			if(gender.equalsIgnoreCase("MALE")){
				updateTotalVoteForVoteReportType(VoteReportTypes.TOTAL_ELECTION_MALE_VOTE, electionId, voteCount);
				return;
			}
			if(gender.equalsIgnoreCase("FEMALE")){
				updateTotalVoteForVoteReportType(VoteReportTypes.TOTAL_ELECTION_FEMALE_VOTE, electionId, voteCount);
				return;
			}
		}
		updateTotalVoteForVoteReportType(VoteReportTypes.TOTAL_ELECTION_NOT_SPECIFIED_VOTE, electionId, voteCount);	
	}
	private void updateTotalVoteForGenderVoteForAssemblyConstituency(Long acId,String gender, int voteCount){
		if(gender != null){
			if(gender.equalsIgnoreCase("MALE")){
				updateTotalVoteForVoteReportType(VoteReportTypes.TOTAL_AC_MALE_VOTE, acId, voteCount);
				return;
			}
			if(gender.equalsIgnoreCase("FEMALE")){
				updateTotalVoteForVoteReportType(VoteReportTypes.TOTAL_AC_FEMALE_VOTE, acId, voteCount);
				return;
			}
		}
		updateTotalVoteForVoteReportType(VoteReportTypes.TOTAL_AC_NOT_SPECIFIED_VOTE, acId, voteCount);	
	}
	private void updateTotalVoteForAgeVote(Long electionId,Date dateOfBirth, Date electionDate, int voteCount){
		if(dateOfBirth == null){
			updateTotalVoteForVoteReportType(VoteReportTypes.TOTAL_ELECTION_AGE_VOTE_UNKNOWN, electionId, voteCount);
			return;
		}
		int age = getAgeAsOn(dateOfBirth, electionDate);
		updateTotalVoteForVoteReportType(VoteReportTypes.TOTAL_ELECTION_AGE_VOTE+"_"+age, electionId, voteCount);
	}
	private void updateTotalVoteForAgeVoteForAssemblyConstituency(Long electionId,Date dateOfBirth, Date electionDate, int voteCount){
		if(dateOfBirth == null){
			updateTotalVoteForVoteReportType(VoteReportTypes.TOTAL_AC_AGE_VOTE_UNKNOWN, electionId, voteCount);
			return;
		}
		int age = getAgeAsOn(dateOfBirth, electionDate);
		updateTotalVoteForVoteReportType(VoteReportTypes.TOTAL_AC_AGE_VOTE+"_"+age, electionId, voteCount);
	}
	private int getAgeAsOn(Date dateOfBirth, Date electionDate) {
		Calendar electionDateCalendar = Calendar.getInstance();
		electionDateCalendar.setTime(electionDate);
		Calendar dateOfBirthCalendar = Calendar.getInstance();
		dateOfBirthCalendar.setTime(dateOfBirth);
	    int diff = electionDateCalendar.get(Calendar.YEAR) - dateOfBirthCalendar.get(Calendar.YEAR);
	    if (dateOfBirthCalendar.get(Calendar.MONTH) > electionDateCalendar.get(Calendar.MONTH) || 
	        (dateOfBirthCalendar.get(Calendar.MONTH) == electionDateCalendar.get(Calendar.MONTH) 
	        && dateOfBirthCalendar.get(Calendar.DATE) > electionDateCalendar.get(Calendar.DATE))) {
	        diff--;
	    }
	    return diff;
	}
	private void updateTotalVoteForElectionPrivateVote(Long electionId, int voteCount){
		updateTotalVoteForVoteReportType(VoteReportTypes.TOTAL_ELECTION_PRIVATE_VOTE, electionId, voteCount);
	}
	private void updateTotalVoteForElectionPartyVote(Long partyId, int voteCount){
		updateTotalVoteForVoteReportType(VoteReportTypes.TOTAL_ELECTION_PARTY_VOTE, partyId, voteCount);
	}
	private void updateTotalVoteForElectionPartyPublicVote(Long partyId, int voteCount){
		updateTotalVoteForVoteReportType(VoteReportTypes.TOTAL_ELECTION_PARTY_PUBLIC_VOTE, partyId, voteCount);
	}
	private void updateTotalVoteForElectionPartyPrivateVote(Long partyId, int voteCount){
		updateTotalVoteForVoteReportType(VoteReportTypes.TOTAL_ELECTION_PARTY_PRIVATE_VOTE, partyId, voteCount);
	}
	private void updateTotalVoteForCandidate(Long candidateId, int voteCount){
		updateTotalVoteForVoteReportType(VoteReportTypes.TOTAL_CANDIDATE_VOTE, candidateId, voteCount);
	}
	private void updateTotalVoteForCandidatePublicVote(Long candidateId, int voteCount){
		updateTotalVoteForVoteReportType(VoteReportTypes.TOTAL_CANDIDATE_PUBLIC_VOTE, candidateId, voteCount);
	}
	private void updateTotalVoteForCandidatePrivateVote(Long candidateId, int voteCount){
		updateTotalVoteForVoteReportType(VoteReportTypes.TOTAL_CANDIDATE_PRIVATE_VOTE, candidateId, voteCount);
	}
	
	private void updateTotalVoteForAc(Long acId, int voteCount){
		updateTotalVoteForVoteReportType(VoteReportTypes.TOTAL_AC_VOTE, acId, voteCount);
	}
	private void updateTotalVoteForAcPublicVote(Long acId, int voteCount){
		updateTotalVoteForVoteReportType(VoteReportTypes.TOTAL_AC_PUBLIC_VOTE, acId, voteCount);
	}
	private void updateTotalVoteForAcPrivateVote(Long acId, int voteCount){
		updateTotalVoteForVoteReportType(VoteReportTypes.TOTAL_AC_PRIVATE_VOTE, acId, voteCount);
	}

	private void updateTotalVoteForVoteReportType(String voteReportType ,Long primaryKey, int voteCount){
		VoteReport voteReport = voteReportDao.getVoteReportByTypeAndPk(voteReportType, primaryKey);
		if(voteReport == null){
			voteReport = new VoteReport();
			voteReport.setReportType(voteReportType);
			voteReport.setPrimaryKey(primaryKey);
			voteReport.setTotalCount(0);
		}
		voteReport.setTotalCount(voteReport.getTotalCount() + voteCount);
		voteReport = voteReportDao.saveVoteReport(voteReport);
	}

	@Override
	@Transactional
	public List<CandidateDto> getCandidatesOfAssmbleConstituency(
			long electionId, long assemblyConstituencyId) {
		List<Candidate> candidareList = candidateDao.getCandidateOfAssemblyConstituency(electionId, assemblyConstituencyId);
		return convertCandidates(candidareList);
	}
	private CandidateDto convertCandidate(Candidate candidate){
		CandidateDto returnCandidate = new CandidateDto();
		BeanUtils.copyProperties(candidate, returnCandidate);
		returnCandidate.setPartyName(candidate.getParty().getName());
		returnCandidate.setPartySymbolUrl(candidate.getParty().getSymbolPicUrl());
		return returnCandidate;
	}
	private List<CandidateDto> convertCandidates(List<Candidate> candidates){
		List<CandidateDto> candidateList = new ArrayList<CandidateDto>(candidates.size());
		for(Candidate oneCandidate : candidates){
			candidateList.add(convertCandidate(oneCandidate));
		}
		return candidateList;
	}

	@Override
	@Transactional
	public List<CandidateDto> getCandidatesOfAssmbleConstituency(
			long electionId, String urlNameOfAc) {
		List<Candidate> candidareList = candidateDao.getCandidateOfAssemblyConstituency(electionId, urlNameOfAc);
		return convertCandidates(candidareList);
	}

	@Override
	@Transactional
	public ElectionDto saveElection(ElectionDto electionDto) {
		Election dbElection = null;
		if(electionDto.getId() != null && electionDto.getId() > 0){
			dbElection = electionDao.getElectionById(electionDto.getId());
			if(dbElection == null){
				throw new VotingException("No such Election exists "+ electionDto.getId());
			}
		}else{
			dbElection = new Election();
		}
		dbElection.setName(electionDto.getName());
		dbElection.setElectionDate(electionDto.getElectionDate());
		dbElection = electionDao.saveElection(dbElection);
		return convertElection(dbElection);
	}
	private ElectionDto convertElection(Election election){
		ElectionDto electionDto = new ElectionDto();
		BeanUtils.copyProperties(election, electionDto);
		return electionDto;
	}
	private List<ElectionDto> convertElections(List<Election> elections){
		List<ElectionDto> returnList = new ArrayList<>(elections.size());
		for(Election oneElection:elections){
			returnList.add(convertElection(oneElection));
		}
		return returnList;
	}

	@Override
	@Transactional
	public List<ElectionDto> getAllElections() {
		List<Election> elections = electionDao.getAllElections();
		return convertElections(elections);
	}

	@Override
	@Transactional
	public PollingStationDto savePollingStation(PollingStationDto pollingStationDto) {
		PollingStation dbPollingStation;
		if(pollingStationDto.getId() == null || pollingStationDto.getId() <= 0){
			dbPollingStation = pollingStationDao.getPollingStationByNameAndAseemblyConstituencyId(pollingStationDto.getAssemblyConstituencyId(), pollingStationDto.getName());
			if(dbPollingStation == null){
				dbPollingStation = new PollingStation();	
			}
		}else{
			dbPollingStation = pollingStationDao.getPollingStationById(pollingStationDto.getId());
			if(dbPollingStation == null){
				throw new VotingException("No such Polling Station exist[id="+pollingStationDto.getId()+"]");
			}
		}
		
		AssemblyConstituency assemblyConstituency = assemblyConstituencyDao.getAssemblyConstituencyById(pollingStationDto.getAssemblyConstituencyId());
		dbPollingStation.setAssemblyConstituency(assemblyConstituency);
		
		dbPollingStation.setName(pollingStationDto.getName());
		dbPollingStation = pollingStationDao.savePollingStation(dbPollingStation);
		
		pollingStationDto.setId(dbPollingStation.getId());
		return pollingStationDto;
	}

	@Override
	@Transactional
	public void createCandidatesForAStateElection(long stateId, long electionId) {
		Election election = electionDao.getElectionById(electionId);
		State state = stateDao.getStateById(stateId);
		List<Party> allParties = partyDao.getAllParties();
		logger.info("Creating Candidates for State "+state.getName() +" for elections "+ election.getName());
		Candidate oneCandidate;
		for(District oneDistrict:state.getDistricts()){
			for(AssemblyConstituency oneAssemblyConstituency:oneDistrict.getAssemblyConstituencies()){
				for(Party oneParty:allParties){
					logger.info("creating Candidate for "+oneParty.getName()+" at "+oneAssemblyConstituency.getName());
					oneCandidate = candidateDao.getPartyCandidateOfAssemblyConstituency(electionId, oneAssemblyConstituency.getId(),oneParty.getId());
					if(oneCandidate != null){
						logger.info("ALready Exists");
						continue;
					}
					logger.info("saving to DB");
					oneCandidate = new Candidate();
					oneCandidate.setAssemblyConstituency(oneAssemblyConstituency);
					oneCandidate.setParty(oneParty);
					oneCandidate.setElection(election);
					oneCandidate.setInfoAvailable(false);
					oneCandidate.setName("Candidate Not Declared");
					
					oneCandidate = candidateDao.saveCandidate(oneCandidate);
				}
			}
		}
		
	}

	@Override
	@Transactional
	public List<AssemblyConstituencyDto> getAllAssemblyConstituenciesOfState(long stateId) {
		List<AssemblyConstituency> allAssemblyConstituencies = assemblyConstituencyDao.getAssemblyConstituencyOfState(stateId);
		List<AssemblyConstituencyDto> returnList = convertAssemblyConstituencies(allAssemblyConstituencies);
		return returnList;
	}

	@Override
	@Transactional
	public StateDto getStateById(Long stateId) {
		State state = stateDao.getStateById(stateId);
		return convertState(state);
	}

	@Override
	@Transactional
	public List<UserVoteDto> getLatestVoteForElection(long electionId) {
		List<VoteMessage> voteMessages = voteMessageDao.getAllVoteMessagesByElection(electionId, 10);
		List<UserVoteDto> latestVotes = new ArrayList<>(voteMessages.size());
		UserVoteDto oneUserVote;
		for(VoteMessage oneVoteMessage:voteMessages){
			oneUserVote = new UserVoteDto();
			oneUserVote.setName(oneVoteMessage.getUserName());
			oneUserVote.setProfilePic(oneVoteMessage.getProfilePic());
			oneUserVote.setVotingTime(oneVoteMessage.getDateModified());
			oneUserVote.setPublicVote(oneVoteMessage.isPublicVote());
			if(oneVoteMessage.isPublicVote()){
				oneUserVote.setVotingMessage(oneVoteMessage.getUserName() + " has voted for "+ oneVoteMessage.getCandidateName()+" of "+oneVoteMessage.getPartyName() +" in Assembly constituency "+oneVoteMessage.getAssemblyConstituencyName());	
			}else{
				oneUserVote.setVotingMessage(oneVoteMessage.getUserName() + " has voted in Assembly constituency "+oneVoteMessage.getAssemblyConstituencyName());
			}
			latestVotes.add(oneUserVote);
		}
		return latestVotes;
	}

	@Override
	@Transactional
	public List<UserVoteDto> getLatestVoteForAssemblyConttituencyAndElection(
			long assemblyConstituencyId, long electionId) {
		List<VoteMessage> voteMessages = voteMessageDao.getAllVoteMessagesByAcAndElection(assemblyConstituencyId, electionId, 10);
		List<UserVoteDto> latestVotes = new ArrayList<>(voteMessages.size());
		UserVoteDto oneUserVote;
		for(VoteMessage oneVoteMessage:voteMessages){
			oneUserVote = new UserVoteDto();
			oneUserVote.setName(oneVoteMessage.getUserName());
			oneUserVote.setProfilePic(oneVoteMessage.getProfilePic());
			oneUserVote.setVotingTime(oneVoteMessage.getVotingTime());
			oneUserVote.setPublicVote(oneVoteMessage.isPublicVote());
			if(oneVoteMessage.isPublicVote()){
					oneUserVote.setVotingMessage(oneVoteMessage.getUserName() + " has voted for "+ oneVoteMessage.getCandidateName()+" of "+oneVoteMessage.getPartyName() );	
			}else{
				oneUserVote.setVotingMessage(oneVoteMessage.getUserName() + " has voted");
			}
			latestVotes.add(oneUserVote);
		}
		return latestVotes;
	}

	@Override
	@Transactional
	public AssemblyConstituencyDto getAssemblyConstituencyById(Long id) {
		AssemblyConstituency assemblyConstituency = assemblyConstituencyDao.getAssemblyConstituencyById(id);
		return convertAssemblyConstituency(assemblyConstituency);
	}

	@Override
	@Transactional
	public AssemblyConstituencyDto getAssemblyConstituencyByUrlname(
			String urlName, long stateId) {
		AssemblyConstituency assemblyConstituency = assemblyConstituencyDao.getAssemblyConstituencyByUrlName(urlName, Constants.DELHI_STATE_ID);
		return convertAssemblyConstituency(assemblyConstituency);
	}

}
