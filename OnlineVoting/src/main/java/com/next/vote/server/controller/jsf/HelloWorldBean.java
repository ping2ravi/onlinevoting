package com.next.vote.server.controller.jsf;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.SessionScoped;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.next.vote.server.persistance.dao.UserDao;
import com.next.vote.server.service.VotingService;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

@Component
@SessionScoped

@URLMappings(mappings = {
        @URLMapping(id = "helloWorldBean", pattern = "/home2/#{message : helloWorldBean.message }", viewId = "/WEB-INF/jsf/home.xhtml")
						})
public class HelloWorldBean implements Serializable{

	private static final long serialVersionUID = 1L;
	private String message = "Hello World 1245";

	@Autowired
	private VotingService votingService;

	@Autowired
	private UserDao userDao;

   public HelloWorldBean() {
      System.out.println("HelloWorld started!");   
   }

	@PostConstruct
	public void init(){
		System.out.println("Hello World 1245 " +message +" , "+ votingService);
	}
	public String getMessage() {
		String m1 = "Hello World 1235 " +message +" , =votingService="+ votingService +" , userDao="+ userDao;
		return m1;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
