package com.next.vote.server.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.google.gdata.util.common.base.StringUtil;
import com.next.vote.client.dto.StateDto;
import com.next.vote.client.dto.UserDto;
import com.next.vote.server.controller.jsf.BaseJsfBean;
import com.next.vote.server.service.VotingService;
import com.next.vote.web.util.VotingSessionUtil;

@Controller
public class LoginController extends BaseController{

	@Autowired
	private VotingService votingService;
	@RequestMapping(value={"/index.html"}, method = RequestMethod.GET)
	public ModelAndView login(ModelAndView mv,HttpServletRequest httpServletRequest){
		passRedirectUrl(httpServletRequest, mv);
		mv.setViewName("loginindex");
		return mv;
	}
	
	@RequestMapping(value="/runcand", method = RequestMethod.GET)
	public ModelAndView init(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, ModelAndView mv) throws Exception {
		votingService.createCandidatesForAStateElection(10L, 1L);
		return mv;
	}
	@RequestMapping(value="/logout", method = RequestMethod.GET)
	public ModelAndView logout(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, ModelAndView mv) throws Exception {
		String redirectUrl = getRedirectUrl(httpServletRequest);
		if(StringUtil.isEmpty(redirectUrl)){
			redirectUrl = "/vote/home";
		}
		httpServletRequest.getSession().invalidate();
		RedirectView redirectView = new RedirectView(redirectUrl);
		mv.setView(redirectView);
		return mv;
	}

}
