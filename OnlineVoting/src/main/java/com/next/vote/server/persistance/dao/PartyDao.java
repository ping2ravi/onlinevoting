package com.next.vote.server.persistance.dao;

import java.util.List;

import com.next.vote.server.persistance.Party;

public interface PartyDao {

	/**
	 * Creates/updates a party in Database
	 * 
	 * @param party
	 * @return saved party
	 * @
	 */
	public abstract Party saveParty(Party party);

	/**
	 * deletes a party in Database
	 * 
	 * @param party
	 * @return updated party
	 * @
	 */
	public abstract void deleteParty(Party party);

	/**
	 * return a Party with given primary key/id
	 * 
	 * @param id
	 * @return Party with PK as id(parameter)
	 * @
	 */
	public abstract Party getPartyById(Long id);

	public abstract List<Party> getAllParties();
	
	
	public abstract Party getPartyByUrlName(String urlName);
	
	public abstract Party getPartyByName(String name);

}