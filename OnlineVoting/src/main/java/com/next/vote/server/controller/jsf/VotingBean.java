package com.next.vote.server.controller.jsf;

import java.util.List;

import javax.faces.bean.ViewScoped;

import org.primefaces.model.chart.PieChartModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.google.gdata.util.common.base.StringUtil;
import com.next.vote.client.dto.AssemblyConstituencyDto;
import com.next.vote.client.dto.CandidateDto;
import com.next.vote.client.dto.CandidateVoteDto;
import com.next.vote.client.dto.GenericVoteDto;
import com.next.vote.client.dto.UserDto;
import com.next.vote.client.dto.UserVoteDto;
import com.next.vote.server.controller.jsf.convertor.CandidateConvertor;
import com.next.vote.server.service.Constants;
import com.next.vote.server.service.VoteReportService;
import com.next.vote.server.service.VotingService;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLActions;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

@Component
@ViewScoped
@URLMappings(mappings = {
        @URLMapping(id = "votingBean", pattern = "/ac/#{votingBean.stateName}/#{votingBean.acName}", viewId = "/WEB-INF/jsf/v2/voting.xhtml")
						})
public class VotingBean extends BaseJsfBean {

	private static final long serialVersionUID = 1L;
	private String stateName;
	private String acName;
	
	private List<CandidateDto> candidates;
	private CandidateDto selectedCandidate;
	private List<CandidateVoteDto> candidateVotes;
	private List<UserVoteDto> userVotes;
	private PieChartModel pieModel; 
	private AssemblyConstituencyDto assemblyConstituency;
	
	private String loadedAcName;
	
	private boolean showResults = false;
	
	private boolean userLoggedIn = false;
	
	private String currentUrl;
	
	@Autowired
	private VotingService votingService;
	
	@Autowired
	private VoteReportService voteReportService;

	private CandidateConvertor candidateConvertor = new CandidateConvertor();
	
	private int oneLineHeight=50;
	private int height;
	private boolean canVote;
	
	@URLActions(actions = {@URLAction(mappingId = "votingBean")})
	public void init() throws Exception {
		currentUrl = "http://www.vote4delhi.com/vote/ac/"+stateName+"/"+acName;
		UserDto loggedInUser = getLoggedInUser();
		if(loggedInUser == null){
			userLoggedIn = false;
		}else{
			userLoggedIn = true;
		}
	    if(acName.equals(loadedAcName)){
	    }else{
	    	loadedAcName = acName;
	    	refreshData();
	    	
	    	candidateConvertor.setCandidates(candidates);
	    	height = (oneLineHeight + 10) * candidates.size();
	    	
	    	
	    	if(loggedInUser == null){
	    		canVote = true;
	    	}else{
		    	if(loggedInUser.getAssemblyConstituencyVotingUrlName() == null ){
		    		canVote = true;
		    	}else{
		    		if(!loggedInUser.getAssemblyConstituencyVotingUrlName().equals(acName)){
		    			sendInfoMessageToJsfScreen("You can not vote for this constituency");
		    			canVote = false;
		    		}else{
		    			canVote = true;
		    		}

		    	}
	    	}
	    	showResults = !canVote;

	    	
	    }
	}
	private void refreshData(){
		assemblyConstituency = votingService.getAssemblyConstituencyByUrlname(acName, Constants.DELHI_STATE_ID);
    	candidates = votingService.getCandidatesOfAssmbleConstituency(Constants.DELHI_ELECTION_ID, assemblyConstituency.getId());
    	for(CandidateDto oneCandidate:candidates){
    		if(StringUtil.isEmpty(oneCandidate.getProfilePic())){
    			oneCandidate.setProfilePic("https://cdn1.iconfinder.com/data/icons/free-large-boss-icon-set/128/Caucasian_boss.png");
    		}
    	}
    	long acId = assemblyConstituency.getId();
    	
    	candidateVotes = voteReportService.getCandidateVotesOfAssemblyConstituency(acId);
    	userVotes = votingService.getLatestVoteForAssemblyConttituencyAndElection(acId, Constants.DELHI_ELECTION_ID);
    	
    	pieModel = new PieChartModel();  
    	for(CandidateVoteDto oneCandidateVoteDto:candidateVotes){
    		pieModel.set(oneCandidateVoteDto.getPartyName(), oneCandidateVoteDto.getTotalVotes());	
    	}
          
	}
	public void votePublically() {
		vote(true);
	}
	public void voteAnonymously() {
		vote(false);
	}
	private void vote(boolean publicVote){
		
		UserDto user = getLoggedInUser(true, buildLoginUrl("/ac/"+stateName+"/"+acName));
		if(user == null){
			return;
		}
		if(selectedCandidate == null){
			sendErrorMessageToJsfScreen("Please select candidate to vote");
		}
		
		if(user == null || user.getAssemblyConstituencyVotingId() == null ){
			System.out.println("User="+user);
			System.out.println("User="+user);
			redirect(buildUrl("/profile"));
		}
		if(isValidInput()){
			try{
				if(user != null){
					String result = votingService.saveVote(user.getId(), selectedCandidate.getId(), publicVote);
					refreshData();
					sendInfoMessageToJsfScreen(result);
					showResults = true;
				}
			}catch(Exception ex){
				sendErrorMessageToJsfScreen("Unable to save your vote at the moment, please try later");
				ex.printStackTrace();
			}
		}
	}

	public void changeMyVote(){
		showResults = false;
	}
	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getAcName() {
		return acName;
	}

	public void setAcName(String acName) {
		this.acName = acName;
	}
	public List<CandidateDto> getCandidates() {
		return candidates;
	}
	public void setCandidates(List<CandidateDto> candidates) {
		this.candidates = candidates;
	}
	public CandidateDto getSelectedCandidate() {
		return selectedCandidate;
	}
	public void setSelectedCandidate(CandidateDto selectedCandidate) {
		this.selectedCandidate = selectedCandidate;
	}
	public CandidateConvertor getCandidateConvertor() {
		return candidateConvertor;
	}
	public void setCandidateConvertor(CandidateConvertor candidateConvertor) {
		this.candidateConvertor = candidateConvertor;
	}
	public int getOneLineHeight() {
		return oneLineHeight;
	}
	public void setOneLineHeight(int oneLineHeight) {
		this.oneLineHeight = oneLineHeight;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public boolean isCanVote() {
		return canVote;
	}
	public void setCanVote(boolean canVote) {
		this.canVote = canVote;
	}
	public List<CandidateVoteDto> getCandidateVotes() {
		return candidateVotes;
	}
	public void setCandidateVotes(List<CandidateVoteDto> candidateVotes) {
		this.candidateVotes = candidateVotes;
	}
	public List<UserVoteDto> getUserVotes() {
		return userVotes;
	}
	public void setUserVotes(List<UserVoteDto> userVotes) {
		this.userVotes = userVotes;
	}
	public PieChartModel getPieModel() {
		return pieModel;
	}
	public void setPieModel(PieChartModel pieModel) {
		this.pieModel = pieModel;
	}
	public AssemblyConstituencyDto getAssemblyConstituency() {
		return assemblyConstituency;
	}
	public void setAssemblyConstituency(AssemblyConstituencyDto assemblyConstituency) {
		this.assemblyConstituency = assemblyConstituency;
	}
	public boolean isShowResults() {
		return showResults;
	}
	public void setShowResults(boolean showResults) {
		this.showResults = showResults;
	}
	public String getCurrentUrl() {
		return currentUrl;
	}
	public void setCurrentUrl(String currentUrl) {
		this.currentUrl = currentUrl;
	}
	public boolean isUserLoggedIn() {
		return userLoggedIn;
	}
	public void setUserLoggedIn(boolean userLoggedIn) {
		this.userLoggedIn = userLoggedIn;
	}

}
