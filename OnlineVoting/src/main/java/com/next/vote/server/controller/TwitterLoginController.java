package com.next.vote.server.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.next.vote.client.dto.UserDto;
import com.next.vote.server.service.VotingService;
import com.next.vote.web.util.VotingSessionUtil;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;

@Controller
public class TwitterLoginController extends BaseController{

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private VotingService votingService;
	
	private  Twitter getNewTwitterInstance(){
		Twitter twitter = new TwitterFactory().getInstance();
		twitter.setOAuthConsumer("ClDBBeBIXUVWwaeTbsl5PQ", "TmpmwjN4G3DrGBabyJcvVaW0GZZHBpbibRvJXllA");
		return twitter;
	}
	
	@RequestMapping(value="/twitter", method = RequestMethod.GET)
	public ModelAndView loginFacebook(ModelAndView mv,HttpServletRequest httpServletRequest){
		System.out.println("Twitter login");
		Twitter twitter = getNewTwitterInstance();
		httpServletRequest.getSession().setAttribute("twitter", twitter);
        try {
            StringBuffer callbackURL = httpServletRequest.getRequestURL();
            int index = callbackURL.lastIndexOf("/");
            callbackURL.replace(index, callbackURL.length(), "").append("/twittercallback.html");
            callbackURL.append("?");
            callbackURL.append(REDIRECT_URL_PARAM_ID);
            callbackURL.append("=");
            callbackURL.append(getRedirectUrl(httpServletRequest));
            RequestToken requestToken = twitter.getOAuthRequestToken(callbackURL.toString());
            httpServletRequest.getSession().setAttribute("requestToken", requestToken);
            System.out.println("requestToken.getAuthenticationURL()="+requestToken.getAuthenticationURL());
            System.out.println("callbackURL="+callbackURL);
            logger.info("requestToken.getAuthenticationURL()="+requestToken.getAuthenticationURL());
            logger.info("requestToken.getAuthorizationURL()="+requestToken.getAuthorizationURL());
            logger.info("requestToken="+requestToken);
            //response.sendRedirect(requestToken.getAuthenticationURL());
            RedirectView rv = new RedirectView(requestToken.getAuthenticationURL());
            rv.setExposeModelAttributes(false);
            mv.setView(rv);
        } catch (TwitterException e) {
            e.printStackTrace();
            logger.error("twitter exception occured",e);
        }
        return mv;
	}
	
	@RequestMapping("/twittercallback.html")
	public ModelAndView twitterCallback(HttpServletRequest httpServletRequest,ModelAndView mv){
		Twitter twitter = (Twitter) httpServletRequest.getSession().getAttribute("twitter");
        RequestToken requestToken = (RequestToken) httpServletRequest.getSession().getAttribute("requestToken");
        String verifier = httpServletRequest.getParameter("oauth_verifier");
        try {
            AccessToken accessToken =  twitter.getOAuthAccessToken(requestToken, verifier);
            httpServletRequest.getSession().removeAttribute("requestToken");
            System.out.println("accessToken="+accessToken);
            System.out.println("ScreenName="+accessToken.getScreenName());
            System.out.println("Token="+accessToken.getToken());
            System.out.println("TokenSecret="+accessToken.getTokenSecret());
            System.out.println("UserId="+accessToken.getUserId());
            
            twitter.setOAuthAccessToken(accessToken);
            System.out.println("Account Settings " + twitter.getAccountSettings());
            
            UserDto user = votingService.saveTwitterAccount(twitter, accessToken);
            
            VotingSessionUtil.saveUserInSession(httpServletRequest, user);
            
        } catch (TwitterException e) {
           e.printStackTrace();
		}
        redirectToViewAfterLogin(httpServletRequest, mv);
        return mv;
	}
	

}
