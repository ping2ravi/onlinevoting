package com.next.vote.server.persistance.dao;

import java.util.List;

import com.next.vote.server.persistance.Election;

public interface ElectionDao {

	/* (non-Javadoc)
	 * @see com.next.aap.server.persistance.dao.impl.ElectionDao#saveElection(com.next.aap.server.persistance.Election)
	 */
	public abstract Election saveElection(Election election);

	/* (non-Javadoc)
	 * @see com.next.aap.server.persistance.dao.impl.ElectionDao#deleteElection(com.next.aap.server.persistance.Election)
	 */
	public abstract void deleteElection(Election election);

	/* (non-Javadoc)
	 * @see com.next.aap.server.persistance.dao.impl.ElectionDao#getElectionById(java.lang.Long)
	 */
	public abstract Election getElectionById(Long id);

	/* (non-Javadoc)
	 * @see com.next.aap.server.persistance.dao.impl.ElectionDao#getAllElections()
	 */
	public abstract List<Election> getAllElections();

}