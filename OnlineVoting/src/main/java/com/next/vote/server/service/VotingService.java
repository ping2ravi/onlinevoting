package com.next.vote.server.service;

import java.util.Date;
import java.util.List;

import twitter4j.Twitter;
import twitter4j.auth.AccessToken;

import com.next.vote.client.dto.AssemblyConstituencyDto;
import com.next.vote.client.dto.CandidateDto;
import com.next.vote.client.dto.DistrictDto;
import com.next.vote.client.dto.ElectionDto;
import com.next.vote.client.dto.PollingStationDto;
import com.next.vote.client.dto.StateDto;
import com.next.vote.client.dto.UserDto;
import com.next.vote.client.dto.UserVoteDto;
import com.next.vote.server.persistance.User;

public interface VotingService {

	
	UserDto saveUser(UserDto user);
	
	UserDto saveFacebookAccount(com.restfb.types.User facebookUser,String token,Date tokenValidTill);
	
	UserDto saveTwitterAccount(Twitter twitter, AccessToken accessToken);
	
	User getUserById(Long id);
	
	List<StateDto> getAllStates() ;
	
	StateDto getStateById(Long stateId) ;
	
	List<DistrictDto> getAllDistrictOfState(long stateId) ;
	
	List<AssemblyConstituencyDto> getAllAssemblyConstituenciesOfDistrict(long districtId) ;
	
	List<AssemblyConstituencyDto> getAllAssemblyConstituenciesOfState(long stateId) ;
	
	StateDto saveState(StateDto stateDto) ;
	
	DistrictDto saveDistrict(DistrictDto districtWeb) ;
	
	PollingStationDto savePollingStation(PollingStationDto pollingStation) ;
	
	List<DistrictDto> saveDistricts(List<DistrictDto> districtWeb) ;
	
	AssemblyConstituencyDto saveAssemblyConstituency(AssemblyConstituencyDto assemblyConstituencyWeb) ;
	
	AssemblyConstituencyDto getAssemblyConstituencyById(Long id) ;
	
	AssemblyConstituencyDto getAssemblyConstituencyByUrlname(String urlName, long stateId) ;
	
	List<AssemblyConstituencyDto> saveAssemblyConstituencies(List<AssemblyConstituencyDto> assemblyConstituencyList) ;
	
	CandidateDto saveCandidate(CandidateDto candidateDto) ;
	
	List<CandidateDto> getAllCandidates();
	
	List<CandidateDto> getCandidatesOfAssmbleConstituency(long electionId,long assemblyConstituencyId);
	
	List<CandidateDto> getCandidatesOfAssmbleConstituency(long electionId,String urlNameOfAc);
	
	String saveVote(Long userId, Long candidateId, boolean publicVote);
	
	ElectionDto saveElection(ElectionDto electionDto);
	
	List<ElectionDto> getAllElections();
	
	void createCandidatesForAStateElection(long stateId,long electionId);
	
	List<UserVoteDto> getLatestVoteForElection(long electionId);
	
	List<UserVoteDto> getLatestVoteForAssemblyConttituencyAndElection(long assemblyConstituencyId, long electionId);
	
	
}
