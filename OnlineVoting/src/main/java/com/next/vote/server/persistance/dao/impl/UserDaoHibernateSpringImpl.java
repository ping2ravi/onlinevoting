package com.next.vote.server.persistance.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.next.vote.server.persistance.User;
import com.next.vote.server.persistance.dao.UserDao;

@Repository
public class UserDaoHibernateSpringImpl extends BaseDaoHibernateSpring<User> implements UserDao{


	@Override
	public User saveUser(User user) {
		super.saveObject(user);
		return user;
	}

	@Override
	public void deleteUser(User user) {
		getCurrentSession().delete(user);
	}

	@Override
	public User getUserById(Long id) {
		return (User)getObjectById(User.class, id);
	}

	@Override
	public List<User> searchUsers() {
		return getAll(User.class);
	}

	@Override
	public List<User> getAllUsers() {
		return getAll(User.class);
	}

	@Override
	public User getUserByEmail(String userEmail) {
		return null;
	}

	@Override
	public User getUserByMobile(String mobile) {
		return null;
	}
}
