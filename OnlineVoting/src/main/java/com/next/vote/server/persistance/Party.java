package com.next.vote.server.persistance;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name="party")
public class Party {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	@Version
	@Column(name="ver")
	private int ver;
	
	@Column(name="date_created")
	private Date dateCreated;
	@Column(name="date_modified")
	private Date dateModified;
	@Column(name="creator_id")
	private Long creatorId;
	@Column(name="modifier_id")
	private Long modifierId;

	
	@Column(name = "name", nullable = false, length=256)
	private String name;
	@Column(name = "name_up", nullable = false, length=256)
	private String nameUp;
	@Column(name = "url_name", nullable = false, length=256)
	private String urlName;

	@Column(name = "symbol_pic_url",  columnDefinition="text")
	private String symbolPicUrl;


	@OneToMany(mappedBy="party")
	private Set<Candidate> candidates;


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public int getVer() {
		return ver;
	}


	public void setVer(int ver) {
		this.ver = ver;
	}


	public Date getDateCreated() {
		return dateCreated;
	}


	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}


	public Date getDateModified() {
		return dateModified;
	}


	public void setDateModified(Date dateModified) {
		this.dateModified = dateModified;
	}


	public Long getCreatorId() {
		return creatorId;
	}


	public void setCreatorId(Long creatorId) {
		this.creatorId = creatorId;
	}


	public Long getModifierId() {
		return modifierId;
	}


	public void setModifierId(Long modifierId) {
		this.modifierId = modifierId;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getNameUp() {
		return nameUp;
	}


	public void setNameUp(String nameUp) {
		this.nameUp = nameUp;
	}


	public String getSymbolPicUrl() {
		return symbolPicUrl;
	}


	public void setSymbolPicUrl(String symbolPicUrl) {
		this.symbolPicUrl = symbolPicUrl;
	}


	public Set<Candidate> getCandidates() {
		return candidates;
	}


	public void setCandidates(Set<Candidate> candidates) {
		this.candidates = candidates;
	}


	public String getUrlName() {
		return urlName;
	}


	public void setUrlName(String urlName) {
		this.urlName = urlName;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Party other = (Party) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
