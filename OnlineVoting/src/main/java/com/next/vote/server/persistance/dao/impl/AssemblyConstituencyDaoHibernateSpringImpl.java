package com.next.vote.server.persistance.dao.impl;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.stereotype.Repository;

import com.google.gdata.util.common.base.StringUtil;
import com.next.vote.server.persistance.AssemblyConstituency;
import com.next.vote.server.persistance.dao.AssemblyConstituencyDao;

@Repository
public class AssemblyConstituencyDaoHibernateSpringImpl extends BaseDaoHibernateSpring<AssemblyConstituency> implements AssemblyConstituencyDao  {

	/* (non-Javadoc)
	 * @see com.next.aap.server.persistance.dao.impl.AssemblyConstituencyDao#saveAssemblyConstituency(com.next.aap.server.persistance.AssemblyConstituency)
	 */
	@Override
	public AssemblyConstituency saveAssemblyConstituency(AssemblyConstituency assemblyConstituency)
	{
		checkIfStringMissing("Name", assemblyConstituency.getName());
		checkIfObjectMissing("District", assemblyConstituency.getDistrict());
		assemblyConstituency.setNameUp(assemblyConstituency.getName().toUpperCase());
		if(StringUtil.isEmpty(assemblyConstituency.getUrlName())){
			assemblyConstituency.setUrlName(getUrlName(assemblyConstituency.getName()));	
		}
		checkIfAssemblyConstituencyExistsWithSameName(assemblyConstituency);
		assemblyConstituency = super.saveObject(assemblyConstituency);
		return assemblyConstituency;
	}

	/* (non-Javadoc)
	 * @see com.next.aap.server.persistance.dao.impl.AssemblyConstituencyDao#deleteAssemblyConstituency(com.next.aap.server.persistance.AssemblyConstituency)
	 */
	@Override
	public void deleteAssemblyConstituency(AssemblyConstituency assemblyConstituency){
		super.deleteObject(assemblyConstituency);
	}

	/* (non-Javadoc)
	 * @see com.next.aap.server.persistance.dao.impl.AssemblyConstituencyDao#getAssemblyConstituencyById(java.lang.Long)
	 */
	@Override
	public AssemblyConstituency getAssemblyConstituencyById(Long id)
	{
		return getObjectById(AssemblyConstituency.class, id);
	}

	/* (non-Javadoc)
	 * @see com.next.aap.server.persistance.dao.impl.AssemblyConstituencyDao#getAllAssemblyConstituencys()
	 */
	@Override
	public List<AssemblyConstituency> getAllAssemblyConstituencys()
	{
		List<AssemblyConstituency> list = executeQueryGetList("from AssemblyConstituency order by name asc");
		return list;
	}
	/* (non-Javadoc)
	 * @see com.next.aap.server.persistance.dao.impl.AssemblyConstituencyDao#getAssemblyConstituencyOfDistrict(long)
	 */
	@Override
	public List<AssemblyConstituency> getAssemblyConstituencyOfDistrict(long districtId){
		Map<String, Object> params = new TreeMap<String, Object>();
		params.put("districtId", districtId);
		List<AssemblyConstituency> list = executeQueryGetList("from AssemblyConstituency where districtId = :districtId order by name asc",params);
		return list;
	}
	private void checkIfAssemblyConstituencyExistsWithSameName(AssemblyConstituency assemblyConstituency){
		AssemblyConstituency existingAssemblyConstituency = null;
		Map<String, Object> params = new TreeMap<String, Object>();
		params.put("districtId", assemblyConstituency.getDistrict().getId());
		params.put("nameUp", assemblyConstituency.getName().toUpperCase());
		if(assemblyConstituency.getId() != null && assemblyConstituency.getId() > 0){
			params.put("id", assemblyConstituency.getId());
			existingAssemblyConstituency = executeQueryGetObject("from AssemblyConstituency where districtId = :districtId and nameUp = :nameUp and id != :id", params);
		}else{
			existingAssemblyConstituency = executeQueryGetObject("from AssemblyConstituency where districtId = :districtId and nameUp = :nameUp", params);
		}

		if(existingAssemblyConstituency != null){
			throw new RuntimeException("AssemblyConstituency already exists with name = "+assemblyConstituency.getName()+" in district "+assemblyConstituency.getDistrict().getName());
		}
		
	}

	@Override
	public AssemblyConstituency getAssemblyConstituencyByUrlName(String urlName, long stateId) {
		Map<String, Object> params = new TreeMap<String, Object>();
		params.put("urlName", urlName.toLowerCase());
		params.put("stateId", stateId);
		AssemblyConstituency assemblyConstituency = executeQueryGetObject("from AssemblyConstituency where urlName = :urlName and district.stateId = :stateId",params);
		return assemblyConstituency;
	}

	@Override
	public AssemblyConstituency getAssemblyConstituencyNameAndDistrictId(
			long districtId, String urlName) {
		Map<String, Object> params = new TreeMap<String, Object>();
		params.put("name", urlName.toUpperCase());
		params.put("districtId", districtId);
		AssemblyConstituency assemblyConstituency = executeQueryGetObject("from AssemblyConstituency where name = :name and districtId=:districtId",params);
		return assemblyConstituency;
	}

	@Override
	public List<AssemblyConstituency> getAssemblyConstituencyOfState(
			long stateId) {
		Map<String, Object> params = new TreeMap<String, Object>();
		params.put("stateId", stateId);
		List<AssemblyConstituency> list = executeQueryGetList("from AssemblyConstituency where district.stateId = :stateId order by name asc",params);
		return list;
	}
}