package com.next.vote.server.controller.jsf.convertor;

import java.io.Serializable;
import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import com.next.vote.client.dto.CandidateDto;


public class CandidateConvertor implements Converter,Serializable{
	private List<CandidateDto> candidates;
	public CandidateConvertor(){
	}
	@Override
	public Object getAsObject(FacesContext facesContext, UIComponent arg1, String submittedValue) {
		if (submittedValue.trim().equals("")) {  
            return null;  
        }
		Long value = Long.parseLong(submittedValue);
		for(CandidateDto oneCandidate:candidates){
			if(oneCandidate.getId().equals(value)){
				return oneCandidate;
			}
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		if(arg2 instanceof CandidateDto){
			if(((CandidateDto)arg2).getId() != null){
				return ((CandidateDto)arg2).getId().toString();	
			}else{
				return "0";
			}
		}
		return arg2.toString();
	}
	public List<CandidateDto> getCandidates() {
		return candidates;
	}
	public void setCandidates(List<CandidateDto> candidates) {
		this.candidates = candidates;
	}

}
