package com.next.vote.server.persistance.dao;

import com.next.vote.server.persistance.PollingStation;

public interface PollingStationDao {

	/**
	 * Creates/updates a pollingStation in Database
	 * 
	 * @param pollingStation
	 * @return saved pollingStation
	 * @throws AppException
	 */
	public abstract PollingStation savePollingStation(
			PollingStation pollingStation);

	/**
	 * deletes a pollingStation in Database
	 * 
	 * @param pollingStation
	 * @return updated pollingStation
	 * @throws AppException
	 */
	public abstract void deletePollingStation(PollingStation pollingStation);

	/**
	 * return a PollingStation with given primary key/id
	 * 
	 * @param id
	 * @return PollingStation with PK as id(parameter)
	 * @throws AppException
	 */
	public abstract PollingStation getPollingStationById(Long id);
	
	
	public abstract PollingStation getPollingStationByNameAndAseemblyConstituencyId(Long acId,String name);

}