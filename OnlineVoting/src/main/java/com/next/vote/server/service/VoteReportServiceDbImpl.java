package com.next.vote.server.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.next.vote.client.dto.CandidateVoteDto;
import com.next.vote.client.dto.GenericVoteDto;
import com.next.vote.client.dto.PartySeatsDto;
import com.next.vote.client.dto.PartyVoteDto;
import com.next.vote.server.persistance.AssemblyConstituency;
import com.next.vote.server.persistance.Candidate;
import com.next.vote.server.persistance.Election;
import com.next.vote.server.persistance.Party;
import com.next.vote.server.persistance.VoteReport;
import com.next.vote.server.persistance.dao.AssemblyConstituencyDao;
import com.next.vote.server.persistance.dao.CandidateDao;
import com.next.vote.server.persistance.dao.DistrictDao;
import com.next.vote.server.persistance.dao.ElectionDao;
import com.next.vote.server.persistance.dao.FacebookAccountDao;
import com.next.vote.server.persistance.dao.PartyDao;
import com.next.vote.server.persistance.dao.PollingStationDao;
import com.next.vote.server.persistance.dao.StateDao;
import com.next.vote.server.persistance.dao.TwitterAccountDao;
import com.next.vote.server.persistance.dao.UserDao;
import com.next.vote.server.persistance.dao.VoteDao;
import com.next.vote.server.persistance.dao.VoteReportDao;

@Service("voteReportService")
public class VoteReportServiceDbImpl implements VoteReportService,Serializable{

	
	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private UserDao userDao;
	@Autowired
	private FacebookAccountDao facebookAccountDao;
	@Autowired
	private StateDao stateDao;
	@Autowired
	private DistrictDao districtDao;
	@Autowired
	private AssemblyConstituencyDao assemblyConstituencyDao;
	@Autowired
	private CandidateDao candidateDao;
	@Autowired
	private TwitterAccountDao twitterAccountDao;
	@Autowired
	private VoteDao voteDao;
	@Autowired
	private ElectionDao electionDao;
	@Autowired
	private PollingStationDao pollingStationDao;
	@Autowired
	private PartyDao partyDao;
	@Autowired
	private VoteReportDao voteReportDao;

	@Override
	@Transactional
	public List<PartyVoteDto> getPartyVotesOfElection(long electionId) {
		List<VoteReport> voteReports = voteReportDao.getVoteReportsByType(VoteReportTypes.TOTAL_ELECTION_PARTY_VOTE);
		List<PartyVoteDto> partyVoteDtos = convertPartyVotes(voteReports);
		return partyVoteDtos;
	}
	
	@Override
	@Transactional
	public List<CandidateVoteDto> getCandidateVotesOfAssemblyConstituency(long assemblyConstituencyId) {
		AssemblyConstituency assemblyConstituency = assemblyConstituencyDao.getAssemblyConstituencyById(assemblyConstituencyId);
		Set<Candidate> acCandidates = assemblyConstituency.getCandidates();
		List<CandidateVoteDto> candidateVoteDtos = new ArrayList<>(acCandidates.size());
		CandidateVoteDto oneCandidateVoteDto;
		VoteReport voteReport;
		int totalVotes = 0;
		for(Candidate oneCandidate:acCandidates){
			oneCandidateVoteDto = new CandidateVoteDto();
			oneCandidateVoteDto.setCandidateId(oneCandidate.getId());
			oneCandidateVoteDto.setCandidateName(oneCandidate.getName());
			oneCandidateVoteDto.setPartyId(oneCandidate.getParty().getId());
			oneCandidateVoteDto.setPartyName(oneCandidate.getParty().getName());
			voteReport = voteReportDao.getVoteReportByTypeAndPk(VoteReportTypes.TOTAL_CANDIDATE_VOTE, oneCandidate.getId());
			if(voteReport == null){
				oneCandidateVoteDto.setTotalVotes(0);
			}else{
				oneCandidateVoteDto.setTotalVotes(voteReport.getTotalCount());
			}
			totalVotes = totalVotes + oneCandidateVoteDto.getTotalVotes();
			candidateVoteDtos.add(oneCandidateVoteDto);
		}
		for(CandidateVoteDto candidateVoteDto:candidateVoteDtos){
			if(totalVotes == 0){
				candidateVoteDto.setVotePercent(0);
			}else{
				candidateVoteDto.setVotePercent(((double)candidateVoteDto.getTotalVotes()/totalVotes)*100);	
			}
		}
		Collections.sort(candidateVoteDtos, new Comparator<CandidateVoteDto>() {
			@Override
			public int compare(CandidateVoteDto o1, CandidateVoteDto o2) {
				return o1.getPartyName().compareTo(o2.getPartyName());
			}
		});
		return candidateVoteDtos;
	}

	
	private List<PartyVoteDto> convertPartyVotes(List<VoteReport> voteReports){
		List<PartyVoteDto> partyVoteDtos = new ArrayList<>(voteReports.size());
		PartyVoteDto onePartyVoteDto;
		Party party;
		List<Party> allParties = partyDao.getAllParties();
		int total = 0;
		for(VoteReport oneVoteReport:voteReports){
			onePartyVoteDto = new PartyVoteDto();
			onePartyVoteDto.setPartyId(oneVoteReport.getPrimaryKey());
			onePartyVoteDto.setTotalVotes(oneVoteReport.getTotalCount());
			party = getParty(allParties, oneVoteReport.getPrimaryKey());
			if(party == null){
				continue;
			}
			onePartyVoteDto.setPartyName(party.getName());
			partyVoteDtos.add(onePartyVoteDto);
			total = total + oneVoteReport.getTotalCount();
		}
		
		for(PartyVoteDto partyVoteDto:partyVoteDtos){
			partyVoteDto.setVotePercent(((double)partyVoteDto.getTotalVotes() / total) * 100);
		}
		return partyVoteDtos;
	}
	private Party getParty(List<Party> allParties, long partyId){
		for(Party oneParty:allParties){
			if(oneParty.getId().equals(partyId)){
				return oneParty;
			}
		}
		return null;
	}

	@Override
	@Transactional
	public List<GenericVoteDto> getPublicPrivateVoteInElection(long electionId) {
		VoteReport privateVoteReport = voteReportDao.getVoteReportByTypeAndPk(VoteReportTypes.TOTAL_ELECTION_PRIVATE_VOTE, electionId);
		VoteReport publicVoteReport = voteReportDao.getVoteReportByTypeAndPk(VoteReportTypes.TOTAL_ELECTION_PUBLIC_VOTE, electionId);

		int total = privateVoteReport.getTotalCount() + publicVoteReport.getTotalCount();
		if(total == 0){
			total = 1;
		}
		List<GenericVoteDto> returnList = new ArrayList<>(2);
		addGenericVote(returnList, "Anonymous Votes", privateVoteReport.getTotalCount(), total);
		addGenericVote(returnList, "Public Votes", publicVoteReport.getTotalCount(), total);
		return returnList;
	}
	private void addGenericVote(List<GenericVoteDto> returnList, String description,int totalVotes, int overallVotes){
		double votePercent = ((double)totalVotes/overallVotes)*100;
		GenericVoteDto genericVoteDto = new GenericVoteDto();
		genericVoteDto.setDescription(description);
		genericVoteDto.setTotalVotes(totalVotes);
		genericVoteDto.setVotePercent(votePercent);
		returnList.add(genericVoteDto);
	}

	@Override
	@Transactional
	public List<GenericVoteDto> getGenderVoteInElection(long electionId) {
		VoteReport maleVoteReport = voteReportDao.getVoteReportByTypeAndPk(VoteReportTypes.TOTAL_ELECTION_MALE_VOTE, electionId);
		VoteReport femaleVoteReport = voteReportDao.getVoteReportByTypeAndPk(VoteReportTypes.TOTAL_ELECTION_FEMALE_VOTE, electionId);
		VoteReport notSpecifiedVoteReport = voteReportDao.getVoteReportByTypeAndPk(VoteReportTypes.TOTAL_ELECTION_NOT_SPECIFIED_VOTE, electionId);

		int total = maleVoteReport.getTotalCount() + femaleVoteReport.getTotalCount() + notSpecifiedVoteReport.getTotalCount();
		if(total == 0){
			total = 1;
		}
		List<GenericVoteDto> returnList = new ArrayList<>(3);
		addGenericVote(returnList, "Males", maleVoteReport.getTotalCount(), total);
		addGenericVote(returnList, "Females", femaleVoteReport.getTotalCount(), total);
		addGenericVote(returnList, "Not Specified", notSpecifiedVoteReport.getTotalCount(), total);
		return returnList;
	}

	@Override
	@Transactional
	public List<GenericVoteDto> getAgeVoteInElection(long electionId) {
		VoteReport unknowAgeVoteReport = voteReportDao.getVoteReportByTypeAndPk(VoteReportTypes.TOTAL_ELECTION_AGE_VOTE_UNKNOWN, electionId);
		//System.out.println("ageDescription="+unknowAgeVoteReport.getReportType()+", totalCount="+unknowAgeVoteReport.getTotalCount());
		List<VoteReport> voteReports = new ArrayList<>(85);
		voteReports.add(unknowAgeVoteReport);
		int totalVotes = 0;
		for(int age=18;age<100;age++){
			VoteReport ageVoteReport = voteReportDao.getVoteReportByTypeAndPk(VoteReportTypes.TOTAL_ELECTION_AGE_VOTE+"_"+age, electionId);
			//System.out.println("ageDescription="+ageVoteReport.getReportType()+", totalCount="+ageVoteReport.getTotalCount());
			voteReports.add(ageVoteReport);
			totalVotes = totalVotes + ageVoteReport.getTotalCount();
		}
		
		List<GenericVoteDto> genericVoteDtos = new ArrayList<>(voteReports.size());
		int ageDescription = 16;
		for(VoteReport oneVoteReport:voteReports){
			ageDescription++;
			if(oneVoteReport.getTotalCount() == 0){
				continue;
			}
			//System.out.println("ageDescription="+ageDescription+", totalCount="+oneVoteReport.getTotalCount());
			if(ageDescription == 17){
				addGenericVote(genericVoteDtos, "Unknow Age", oneVoteReport.getTotalCount(), totalVotes);	
			}else{
				addGenericVote(genericVoteDtos, ageDescription +" Years", oneVoteReport.getTotalCount(), totalVotes);
			}
		}
		return genericVoteDtos;
	}

	@Override
	@Transactional
	public List<GenericVoteDto> getPublicPrivateVoteInAssemblyConstituency(
			long acId) {
		VoteReport privateVoteReport = voteReportDao.getVoteReportByTypeAndPk(VoteReportTypes.TOTAL_AC_PRIVATE_VOTE, acId);
		VoteReport publicVoteReport = voteReportDao.getVoteReportByTypeAndPk(VoteReportTypes.TOTAL_AC_PUBLIC_VOTE, acId);

		int total = privateVoteReport.getTotalCount() + publicVoteReport.getTotalCount();
		if(total == 0){
			total = 1;
		}
		List<GenericVoteDto> returnList = new ArrayList<>(2);
		addGenericVote(returnList, "Anonymous Votes", privateVoteReport.getTotalCount(), total);
		addGenericVote(returnList, "Public Votes", publicVoteReport.getTotalCount(), total);
		return returnList;
	}

	@Override
	@Transactional
	public List<GenericVoteDto> getGenderVoteInAssemblyConstituency(long acId) {
		VoteReport maleVoteReport = voteReportDao.getVoteReportByTypeAndPk(VoteReportTypes.TOTAL_AC_MALE_VOTE, acId);
		VoteReport femaleVoteReport = voteReportDao.getVoteReportByTypeAndPk(VoteReportTypes.TOTAL_AC_FEMALE_VOTE, acId);
		VoteReport notSpecifiedVoteReport = voteReportDao.getVoteReportByTypeAndPk(VoteReportTypes.TOTAL_AC_NOT_SPECIFIED_VOTE, acId);

		int total = maleVoteReport.getTotalCount() + femaleVoteReport.getTotalCount() + notSpecifiedVoteReport.getTotalCount();
		if(total == 0){
			total = 1;
		}
		List<GenericVoteDto> returnList = new ArrayList<>(3);
		addGenericVote(returnList, "Males", maleVoteReport.getTotalCount(), total);
		addGenericVote(returnList, "Females", femaleVoteReport.getTotalCount(), total);
		addGenericVote(returnList, "Not Specified", notSpecifiedVoteReport.getTotalCount(), total);
		return returnList;
	}

	@Override
	@Transactional
	public List<GenericVoteDto> getAgeVoteInAssemblyConstituency(long acId) {
		VoteReport unknowAgeVoteReport = voteReportDao.getVoteReportByTypeAndPk(VoteReportTypes.TOTAL_AC_AGE_VOTE_UNKNOWN, acId);
		//System.out.println("ageDescription="+unknowAgeVoteReport.getReportType()+", totalCount="+unknowAgeVoteReport.getTotalCount());
		List<VoteReport> voteReports = new ArrayList<>(85);
		voteReports.add(unknowAgeVoteReport);
		int totalVotes = 0;
		for(int age=18;age<100;age++){
			VoteReport ageVoteReport = voteReportDao.getVoteReportByTypeAndPk(VoteReportTypes.TOTAL_AC_AGE_VOTE+"_"+age, acId);
			//System.out.println("ageDescription="+ageVoteReport.getReportType()+", totalCount="+ageVoteReport.getTotalCount());
			voteReports.add(ageVoteReport);
			totalVotes = totalVotes + ageVoteReport.getTotalCount();
		}
		
		List<GenericVoteDto> genericVoteDtos = new ArrayList<>(voteReports.size());
		int ageDescription = 16;
		for(VoteReport oneVoteReport:voteReports){
			ageDescription++;
			if(oneVoteReport.getTotalCount() == 0){
				continue;
			}
			//System.out.println("ageDescription="+ageDescription+", totalCount="+oneVoteReport.getTotalCount());
			if(ageDescription == 17){
				addGenericVote(genericVoteDtos, "Unknow Age", oneVoteReport.getTotalCount(), totalVotes);	
			}else{
				addGenericVote(genericVoteDtos, ageDescription +" Years", oneVoteReport.getTotalCount(), totalVotes);
			}
		}
		return genericVoteDtos;
	}

	@Override
	@Transactional
	public List<PartySeatsDto> getPartySeatsByElectionId(long electionId) {
		// TODO Auto-generated method stub
		Election election = electionDao.getElectionById(electionId);
		Set<Candidate> electionCandidates = election.getCandidates();
		
		Map<Long, Map<Candidate, Integer>> voteMap = new HashMap<>();
		Map<Candidate, Integer> acVoteMap;
		VoteReport voteReport;
		Map<Long, Integer> voteReports = new HashMap<>();
		
		List<VoteReport> allVoteReportsOfCandidate = voteReportDao.getVoteReportsByType(VoteReportTypes.TOTAL_CANDIDATE_VOTE);
		
		for(VoteReport oneVoteReport : allVoteReportsOfCandidate){
			//System.out.println("oneVoteReport = "+oneVoteReport.getPrimaryKey() +" , " +oneVoteReport.getTotalCount());
			voteReports.put(oneVoteReport.getPrimaryKey(), oneVoteReport.getTotalCount());
		}
		for(Candidate oneCandidate:electionCandidates){
			acVoteMap = voteMap.get(oneCandidate.getAssemblyConstituencyId());
			if(acVoteMap == null){
				acVoteMap = new HashMap<Candidate, Integer>();
				voteMap.put(oneCandidate.getAssemblyConstituencyId(), acVoteMap);
			}
			if(voteReports.get(oneCandidate.getId()) == null){
				acVoteMap.put(oneCandidate, 0);	
			}else{
				acVoteMap.put(oneCandidate, voteReports.get(oneCandidate.getId()));
			}
			
		}
		
		Map<Party, Integer> partySeats = new HashMap<>();
		Party winningPartyForAc;
		Integer winningPartyVotes;
		for(Entry<Long, Map<Candidate, Integer>> oneEntry:voteMap.entrySet()){
			winningPartyForAc = null;
			winningPartyVotes = 0;
			//System.out.println("AC = "+oneEntry.getKey());
			for(Entry<Candidate, Integer> oneCandidateEntry:oneEntry.getValue().entrySet()){
				if(oneCandidateEntry.getValue() > 0){
					if(oneCandidateEntry.getValue() > winningPartyVotes){
						winningPartyVotes = oneCandidateEntry.getValue();
						winningPartyForAc = oneCandidateEntry.getKey().getParty();
						//System.out.println(winningPartyForAc.getName() +" got "+ winningPartyVotes+" votes");
					}
				}
			}
			if(winningPartyForAc != null){
				Integer partySeatsCount = partySeats.get(winningPartyForAc);
				if(partySeatsCount == null){
					partySeatsCount = 0;
				}
				partySeatsCount++;
				partySeats.put(winningPartyForAc, partySeatsCount);
			}
		}
		
		List<PartySeatsDto> returnList = new ArrayList<>();
		PartySeatsDto onePartySeatsDto;
		for(Entry<Party, Integer> onePartyVoteCountEntry:partySeats.entrySet()){
			//System.out.println(onePartyVoteCountEntry.getKey().getName() +" have got "+ onePartyVoteCountEntry.getValue()+" votes");
			onePartySeatsDto = new PartySeatsDto();
			onePartySeatsDto.setPartyId(onePartyVoteCountEntry.getKey().getId());
			onePartySeatsDto.setPartyName(onePartyVoteCountEntry.getKey().getName());
			onePartySeatsDto.setTotalSeats(onePartyVoteCountEntry.getValue());
			returnList.add(onePartySeatsDto);
		}
		return returnList;
	}

}
