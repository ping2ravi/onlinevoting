package com.next.vote.server.service;

import java.util.List;

import com.next.vote.client.dto.CandidateVoteDto;
import com.next.vote.client.dto.GenericVoteDto;
import com.next.vote.client.dto.PartySeatsDto;
import com.next.vote.client.dto.PartyVoteDto;


public interface VoteReportService {

	List<PartyVoteDto> getPartyVotesOfElection(long electionId);
	
	List<CandidateVoteDto> getCandidateVotesOfAssemblyConstituency(long assemblyConstituencyId);
	
	
	List<GenericVoteDto> getPublicPrivateVoteInElection(long electionId);
	
	
	List<GenericVoteDto> getGenderVoteInElection(long electionId);
	
	
	List<GenericVoteDto> getAgeVoteInElection(long electionId);

	
	List<GenericVoteDto> getPublicPrivateVoteInAssemblyConstituency(long acId);
	
	
	List<GenericVoteDto> getGenderVoteInAssemblyConstituency(long acId);
	
	
	List<GenericVoteDto> getAgeVoteInAssemblyConstituency(long acId);
	
	List<PartySeatsDto> getPartySeatsByElectionId(long electionId);


}
