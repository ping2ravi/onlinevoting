package com.next.vote.server.persistance.dao.impl;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gdata.util.common.base.StringUtil;

public class BaseDaoHibernateSpring<T> implements Serializable{

	private static final long serialVersionUID = 1L;
	@Autowired
	private SessionFactory sessionFactory;
	protected Logger logger = LoggerFactory.getLogger(this.getClass());

	protected Session getCurrentSession(){
		return sessionFactory.getCurrentSession();
	}
	protected T saveObject(T object){
		this.sessionFactory.getCurrentSession().save(object);
		return object;
	}
	protected void deleteObject(T object){
		this.sessionFactory.getCurrentSession().delete(object);
	}
	@SuppressWarnings("unchecked")
	protected T getObjectById(Class<T> type,long id){
		return (T)getCurrentSession().load(type, id);
	}
	@SuppressWarnings("unchecked")
	public List<T> getAll(Class<T> abc){
		String className = getClassName(abc.toString());
		return this.sessionFactory.getCurrentSession().createQuery("from "+className).list();
	}
	@SuppressWarnings("unchecked")
	public T executeQueryGetObject(String query){
		return (T)this.sessionFactory.getCurrentSession().createQuery(query).uniqueResult();
	}
	@SuppressWarnings("unchecked")
	public T executeQueryGetObject(String query,Map<String, Object> params){
		Query hibernateQuery = this.sessionFactory.getCurrentSession().createQuery(query);
		for(Entry<String, Object> oneEntry:params.entrySet()){
			hibernateQuery.setParameter(oneEntry.getKey(), oneEntry.getValue());
		}
		return (T)hibernateQuery.uniqueResult();
	}
	@SuppressWarnings("unchecked")
	public List<T> executeQueryGetList(String query){
		return (List<T>)this.sessionFactory.getCurrentSession().createQuery(query).list();
	}
	@SuppressWarnings("unchecked")
	public List<T> executeQueryGetList(String query,Map<String, Object> params){
		return executeQueryGetList(query, params, null);
	}
	@SuppressWarnings("unchecked")
	public List<T> executeQueryGetList(String query,Map<String, Object> params, Integer pageSize){
		Query hibernateQuery = this.sessionFactory.getCurrentSession().createQuery(query);
		for(Entry<String, Object> oneEntry:params.entrySet()){
			hibernateQuery.setParameter(oneEntry.getKey(), oneEntry.getValue());
		}
		if(pageSize != null && pageSize > 0){
			hibernateQuery.setFetchSize(pageSize);
			hibernateQuery.setMaxResults(pageSize);
		}
		return (List<T>)hibernateQuery.list();
	}
	public String getOnlyName(Class<T> abc){
		String className = getClassName(abc.toString());
		return className;
	}
	protected String getClassName(String fullName){
		return fullName.substring(fullName.lastIndexOf(".") + 1);
	}
	protected void checkIfStringMissing(String name,String value){
		if(StringUtil.isEmpty(value)){
			throw new RuntimeException(name +" can not be null or empty");
		}
	}
	protected void checkIfObjectMissing(String name,Object value){
		if(value == null){
			throw new RuntimeException(name +" can not be null");
		}
	}
	
	protected static String getUrlName(String name){
		if(name == null){
			return null;
		}
		name = name.toLowerCase();
		name = StringUtil.replaceChars(name, " ", '_');
		name = StringUtil.replaceChars(name, "\"", '_');
		name = StringUtil.replaceChars(name, "'", '_');
		name = StringUtil.replaceChars(name, "&", '_');
		name = StringUtil.replaceChars(name, "?", '_');
		name = StringUtil.replaceChars(name, "%", '_');
		
		return name;
	}
	public static void main(String[] args){
		System.out.println(getUrlName("i a'int wo\"r'k'ing"));
	}

}
