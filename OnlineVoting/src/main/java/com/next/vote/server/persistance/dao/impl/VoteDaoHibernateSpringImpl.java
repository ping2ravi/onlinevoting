package com.next.vote.server.persistance.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.next.vote.server.persistance.Vote;
import com.next.vote.server.persistance.dao.VoteDao;

@Repository
public class VoteDaoHibernateSpringImpl extends BaseDaoHibernateSpring<Vote> implements VoteDao  {

	/* (non-Javadoc)
	 * @see com.next.vote.server.persistance.dao.impl.VoreDao#saveVote(com.next.vote.server.persistance.Vote)
	 */
	@Override
	public Vote saveVote(Vote vote) 
	{
		checkIfObjectMissing("Assembly Constituency", vote.getAssemblyConstituency());
		checkIfObjectMissing("Candidate", vote.getCandidate());
		checkIfObjectMissing("Party", vote.getParty());
		checkIfObjectMissing("User", vote.getUser());
		vote = super.saveObject(vote);
		return vote;
	}
	

	/* (non-Javadoc)
	 * @see com.next.vote.server.persistance.dao.impl.VoreDao#deleteVote(com.next.vote.server.persistance.Vote)
	 */
	@Override
	public void deleteVote(Vote vote)  {
		super.deleteObject(vote);
	}

	/* (non-Javadoc)
	 * @see com.next.vote.server.persistance.dao.impl.VoreDao#getVoteById(java.lang.Long)
	 */
	@Override
	public Vote getVoteById(Long id) 
	{
		Vote vote = super.getObjectById(Vote.class, id);
		return vote;
	}

	/* (non-Javadoc)
	 * @see com.next.vote.server.persistance.dao.impl.VoreDao#getAllVotes()
	 */
	@Override
	public List<Vote> getAllVotes() 
	{
		List<Vote> list = executeQueryGetList("from Vote");
		return list;
	}
	
	/* (non-Javadoc)
	 * @see com.next.vote.server.persistance.dao.impl.VoreDao#getAllVotesByAc(java.lang.Long)
	 */
	@Override
	public List<Vote> getAllVotesByAc(Long assemblyConstituencyId) 
	{
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("assemblyConstituencyId", assemblyConstituencyId);
		List<Vote> list = executeQueryGetList("from Vote where assemblyConstituencyId = :assemblyConstituencyId", params);
		return list;
	}

	/* (non-Javadoc)
	 * @see com.next.vote.server.persistance.dao.impl.VoreDao#getAllVotesByCandidate(java.lang.Long)
	 */
	@Override
	public List<Vote> getAllVotesByCandidate(Long candidateId) 
	{
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("candidateId", candidateId);
		List<Vote> list = executeQueryGetList("from Vote where candidateId = :candidateId", params);
		return list;
	}

	/* (non-Javadoc)
	 * @see com.next.vote.server.persistance.dao.impl.VoreDao#getAllVotesByParty(java.lang.Long)
	 */
	@Override
	public List<Vote> getAllVotesByParty(Long partyId) 
	{
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("partyId", partyId);
		List<Vote> list = executeQueryGetList("from Vote where partyId = :partyId", params);
		return list;
	}


	@Override
	public Vote getVotesByUserForElection(Long userId, Long electionId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("userId", userId);
		params.put("electionId", electionId);
		Vote vote = executeQueryGetObject("from Vote where userId = :userId and electionId=:electionId", params);
		return vote;
	}


	@Override
	public List<Vote> getAllVotesByElection(Long electionId, int pageSize) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("electionId", electionId);
		List<Vote> list = executeQueryGetList("from Vote where electionId = :electionId order by dateModified desc", params, pageSize);
		return list;
	}


	@Override
	public List<Vote> getAllVotesByAcAndElection(Long assemblyConstituencyId,Long electionId,  int pageSize) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("assemblyConstituencyId", assemblyConstituencyId);
		params.put("electionId", electionId);
		List<Vote> list = executeQueryGetList("from Vote where assemblyConstituencyId = :assemblyConstituencyId and electionId = :electionId order by dateModified desc", params, pageSize);
		return list;
	}

}