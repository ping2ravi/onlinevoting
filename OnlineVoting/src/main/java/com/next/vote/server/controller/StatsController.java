package com.next.vote.server.controller;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.next.vote.client.dto.AssemblyConstituencyDto;
import com.next.vote.client.dto.CandidateVoteDto;
import com.next.vote.client.dto.GenericVoteDto;
import com.next.vote.client.dto.PartySeatsDto;
import com.next.vote.client.dto.PartyVoteDto;
import com.next.vote.client.dto.UserVoteDto;
import com.next.vote.server.service.Constants;
import com.next.vote.server.service.VoteReportService;
import com.next.vote.server.service.VotingService;

@Controller
public class StatsController extends BaseController {

	@Autowired
	private VotingService votingService;
	@Autowired
	private VoteReportService voteReportService;
	
	@RequestMapping(value="/stats/delhi", method = RequestMethod.GET)
	public ModelAndView showDelhiStats(HttpServletRequest httpServletRequest, ModelAndView mv) throws Exception {
		List<AssemblyConstituencyDto> assemblyConstituencyDtos = votingService.getAllAssemblyConstituenciesOfState(10L);
		List<GenericVoteDto> publicPrivateVote = voteReportService.getPublicPrivateVoteInElection(Constants.DELHI_ELECTION_ID);
		List<GenericVoteDto> genderVotes = voteReportService.getGenderVoteInElection(Constants.DELHI_ELECTION_ID);
		List<GenericVoteDto> ageVotes = voteReportService.getAgeVoteInElection(Constants.DELHI_ELECTION_ID);
		List<PartySeatsDto> partySeats = voteReportService.getPartySeatsByElectionId(Constants.DELHI_ELECTION_ID);
		List<PartyVoteDto> partyVoteDtos = voteReportService.getPartyVotesOfElection(Constants.DELHI_ELECTION_ID);
		Collections.sort(partyVoteDtos, new Comparator<PartyVoteDto>() {
			@Override
			public int compare(PartyVoteDto o1, PartyVoteDto o2) {
				return o1.getPartyName().compareTo(o2.getPartyName());
			}
		});
		
		
		mv.getModel().put("partyVotelist", partyVoteDtos);
		mv.getModel().put("version", defaultVersion);
		mv.getModel().put("context", httpServletRequest.getServletContext().getContextPath());

		mv.getModel().put("aclist", assemblyConstituencyDtos);
		mv.getModel().put("publicPrivateVote", publicPrivateVote);
		mv.getModel().put("genderVotes", genderVotes);
		mv.getModel().put("ageVotes", ageVotes);
		mv.getModel().put("partySeats", partySeats);
		mv.getModel().put("LoggedInUser", getLoggedInUser(httpServletRequest));
		mv.getModel().put("CurrentUrl", getCurrentUrl(httpServletRequest));
		mv.setViewName("v2/delhistats");
		return mv;
	}
	
	
	@RequestMapping(value="/delhi/{acName}", method = RequestMethod.GET)
	public ModelAndView showAcStats(HttpServletRequest httpServletRequest, ModelAndView mv, @PathVariable String acName) throws Exception {
		mv.getModel().put("version", defaultVersion);
		mv.getModel().put("context", httpServletRequest.getServletContext().getContextPath());

		AssemblyConstituencyDto assemblyConstituencyDto = votingService.getAssemblyConstituencyByUrlname(acName, Constants.DELHI_STATE_ID);
		List<AssemblyConstituencyDto> assemblyConstituencyDtos = votingService.getAllAssemblyConstituenciesOfState(10L);
		mv.getModel().put("aclist", assemblyConstituencyDtos);
		
		if(assemblyConstituencyDto == null){
			mv.getModel().put("error", "Not a valid assembly Constituency");
		}else{
			long acId = assemblyConstituencyDto.getId();
			List<GenericVoteDto> genderVotes = voteReportService.getGenderVoteInAssemblyConstituency(acId);
			List<GenericVoteDto> publicPrivateVote = voteReportService.getPublicPrivateVoteInAssemblyConstituency(acId);
			List<GenericVoteDto> ageVotes = voteReportService.getAgeVoteInAssemblyConstituency(acId);
			List<CandidateVoteDto> candidateVotes = voteReportService.getCandidateVotesOfAssemblyConstituency(acId);
			List<UserVoteDto> latestVotes = votingService.getLatestVoteForAssemblyConttituencyAndElection(acId, Constants.DELHI_ELECTION_ID);

			mv.getModel().put("latestVotes", latestVotes);
			
			mv.getModel().put("assemblyConstituency", assemblyConstituencyDto);
			mv.getModel().put("genderVotes", genderVotes);
			mv.getModel().put("publicPrivateVote", publicPrivateVote);
			mv.getModel().put("ageVotes", ageVotes);
			mv.getModel().put("candidateVotes", candidateVotes);
			mv.getModel().put("selectedAcId", acId);
			mv.getModel().put("LoggedInUser", getLoggedInUser(httpServletRequest));
			mv.getModel().put("CurrentUrl", getCurrentUrl(httpServletRequest));
		}
		mv.setViewName("v2/delhiac");
		return mv;
	}
}
