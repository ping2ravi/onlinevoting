package com.next.vote.server.persistance.dao;

import java.util.List;

import com.next.vote.server.persistance.Candidate;

public interface CandidateDao {

	/**
	 * Creates/updates a candidate in Database
	 * 
	 * @param candidate
	 * @return saved candidate
	 * @throws AppException
	 */
	public abstract Candidate saveCandidate(Candidate candidate);

	/**
	 * deletes a candidate in Database
	 * 
	 * @param candidate
	 * @return updated candidate
	 * @throws AppException
	 */
	public abstract void deleteCandidate(Candidate candidate);

	/**
	 * return a Candidate with given primary key/id
	 * 
	 * @param id
	 * @return Candidate with PK as id(parameter)
	 * @throws AppException
	 */
	public abstract Candidate getCandidateById(Long id);

	public abstract List<Candidate> getAllCandidates();

	public abstract List<Candidate> getCandidateOfAssemblyConstituency(long electionId,long assemblyConstituencyId);
	
	public abstract Candidate getPartyCandidateOfAssemblyConstituency(long electionId,long assemblyConstituencyId, long partyId);

	public abstract List<Candidate> getCandidateOfAssemblyConstituency(long electionId,
			String urlNameOfassemblyConstituency);

	public abstract List<Candidate> getAllCandidatesOfParty(long electionId,
			long partyId);
}