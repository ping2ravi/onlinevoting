package com.next.vote.server.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.next.vote.client.dto.StateDto;
import com.next.vote.client.dto.UserDto;
import com.next.vote.server.controller.jsf.BaseJsfBean;
import com.next.vote.server.service.VotingService;
import com.next.vote.web.util.VotingSessionUtil;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLActions;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

@Controller
public class MyAssemblyConstituencyController extends BaseController {

	private static final long serialVersionUID = 1L;

	@Autowired
	private VotingService votingService;

	@RequestMapping(value="/myconstituency", method = RequestMethod.GET)
	public ModelAndView init(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, ModelAndView mv) throws Exception {
		UserDto loggedInuser = VotingSessionUtil.getUserFromSession(httpServletRequest);
		if(loggedInuser == null){
			setFinalRedirectUrlInSesion(httpServletRequest, "/myconstituency");
			String redirectUrl = BaseJsfBean.buildLoginUrl(httpServletRequest, "/myconstituency");
			redirectToViewAfterLogin(redirectUrl, mv);
		}else{
			if(loggedInuser.getAssemblyConstituencyVotingUrlName() == null){
				setFinalRedirectUrlInSesion(httpServletRequest, "/myconstituency");
				String redirectUrl = httpServletRequest.getContextPath()+"/profile";
				redirectToViewAfterLogin(redirectUrl, mv);
			}else{
				//StateDto stateDto = votingService.getStateById(loggedInuser.getStateVotingId());
				//String redirectUrl = httpServletRequest.getContextPath()+"/ac/"+stateDto.getName()+"/"+loggedInuser.getAssemblyConstituencyVotingUrlName();
				String redirectUrl = httpServletRequest.getContextPath()+"/ac/Delhi/"+loggedInuser.getAssemblyConstituencyVotingUrlName();
				redirectToViewAfterLogin(redirectUrl, mv);
			}
		}
		return mv;
		
	}


}
