package com.next.vote.server.controller;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.next.vote.client.dto.AssemblyConstituencyDto;
import com.next.vote.client.dto.GenericVoteDto;
import com.next.vote.client.dto.PartySeatsDto;
import com.next.vote.client.dto.PartyVoteDto;
import com.next.vote.client.dto.UserVoteDto;
import com.next.vote.server.service.Constants;
import com.next.vote.server.service.VoteReportService;
import com.next.vote.server.service.VotingService;

@Controller
public class IndexController extends BaseController{

	@Autowired
	private VotingService votingService;
	@Autowired
	private VoteReportService voteReportService;

	@RequestMapping(value={"/{version}/main"}, method = RequestMethod.GET)
	public ModelAndView showIndexPage(ModelAndView mv,HttpServletRequest httpServletRequest, @PathVariable String version){
		return generatePage(mv, httpServletRequest, version);
	}
	@RequestMapping(value={"/home"}, method = RequestMethod.GET)
	public ModelAndView showIndexPage(ModelAndView mv,HttpServletRequest httpServletRequest){
		return generatePage(mv, httpServletRequest,defaultVersion);
	}
	
	public ModelAndView generatePage(ModelAndView mv,HttpServletRequest httpServletRequest,String version){
		mv.getModel().put("LoggedInUser", getLoggedInUser(httpServletRequest));
		mv.getModel().put("CurrentUrl", getCurrentUrl(httpServletRequest));
		mv.getModel().put("version", version);
		mv.getModel().put("context", httpServletRequest.getServletContext().getContextPath());
		/*
		List<AssemblyConstituencyDto> assemblyConstituencyDtos = votingService.getAllAssemblyConstituenciesOfState(10L);
		for(AssemblyConstituencyDto oneAssemblyConstituencyDto:assemblyConstituencyDtos){
			System.out.println(oneAssemblyConstituencyDto);
		}
		*/
		List<PartyVoteDto> partyVoteDtos = voteReportService.getPartyVotesOfElection(Constants.DELHI_ELECTION_ID);
		Collections.sort(partyVoteDtos, new Comparator<PartyVoteDto>() {
			@Override
			public int compare(PartyVoteDto o1, PartyVoteDto o2) {
				return o1.getPartyName().compareTo(o2.getPartyName());
			}
		});
		
		
		List<UserVoteDto> latestVotes = votingService.getLatestVoteForElection(Constants.DELHI_ELECTION_ID);
		mv.getModel().put("partyVotelist", partyVoteDtos);
		mv.getModel().put("latestVotes", latestVotes);
		passRedirectUrl(httpServletRequest, mv);
		mv.setViewName(version+"/index");
		
		return mv;
	}
}
