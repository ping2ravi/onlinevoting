package com.next.vote.server.controller.jsf;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.next.vote.server.service.VotingService;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLActions;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

@Component
@Scope("session")
@URLMappings(mappings = {
        @URLMapping(id = "electionBean", pattern = "/admin/elections", viewId = "/WEB-INF/jsf/voting.xhtml")
						})
public class ElectionBean implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Autowired
	private VotingService votingService;

	@URLActions(actions = {@URLAction(mappingId = "electionBean")})
	public void init() throws Exception {
	    System.out.println("<--init");
	    System.out.println("Will Load from DB");
	    System.out.println("init-->");
	}
	public void refreshElectionList(){
		
	}

}
