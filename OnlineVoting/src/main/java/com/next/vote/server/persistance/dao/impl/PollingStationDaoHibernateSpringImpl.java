package com.next.vote.server.persistance.dao.impl;

import java.util.Map;
import java.util.TreeMap;

import org.springframework.stereotype.Component;

import com.next.vote.server.persistance.PollingStation;
import com.next.vote.server.persistance.dao.PollingStationDao;

@Component
public class PollingStationDaoHibernateSpringImpl extends BaseDaoHibernateSpring<PollingStation> implements PollingStationDao  {

	/* (non-Javadoc)
	 * @see com.next.vote.server.persistance.dao.impl.PollingStationDao#savePollingStation(com.next.vote.server.persistance.PollingStation)
	 */
	@Override
	public PollingStation savePollingStation(PollingStation pollingStation)
	{
		checkIfStringMissing("Name", pollingStation.getName());
		checkIfObjectMissing("AssemblyConstituency", pollingStation.getAssemblyConstituency());
		checkIfPollingStationExistsWithSameName(pollingStation);
		pollingStation.setNameUp(pollingStation.getName().toUpperCase());
		pollingStation = super.saveObject(pollingStation);
		return pollingStation;
	}

	/* (non-Javadoc)
	 * @see com.next.vote.server.persistance.dao.impl.PollingStationDao#deletePollingStation(com.next.vote.server.persistance.PollingStation)
	 */
	@Override
	public void deletePollingStation(PollingStation pollingStation) {
		super.deleteObject(pollingStation);
	}

	/* (non-Javadoc)
	 * @see com.next.vote.server.persistance.dao.impl.PollingStationDao#getPollingStationById(java.lang.Long)
	 */
	@Override
	public PollingStation getPollingStationById(Long id)
	{
		PollingStation pollingStation = super.getObjectById(PollingStation.class, id);
		return pollingStation;
	}

	
	private void checkIfPollingStationExistsWithSameName(PollingStation pollingStation){
		Map<String, Object> params = new TreeMap<String, Object>();
		params.put("assemblyConstituencyId", pollingStation.getAssemblyConstituency().getId());
		params.put("nameUp", pollingStation.getName().toUpperCase());
		PollingStation existingPollingStation = null;
		if(pollingStation.getId() != null && pollingStation.getId() > 0){
			params.put("id", pollingStation.getId());
			existingPollingStation = executeQueryGetObject("from PollingStation where assemblyConstituencyId=:assemblyConstituencyId and nameUp= :nameUp and id != :id",params);
		}else{
			existingPollingStation = executeQueryGetObject("from PollingStation where assemblyConstituencyId=:assemblyConstituencyId and nameUp= :nameUp",params);
		}
		if(existingPollingStation != null){
			throw new RuntimeException("PollingStation already exists with name = "+existingPollingStation.getName()+" in Assembly Constituency "+existingPollingStation.getAssemblyConstituency().getName());
		}
	}

	@Override
	public PollingStation getPollingStationByNameAndAseemblyConstituencyId(
			Long assemblyConstituencyId, String name) {
		Map<String, Object> params = new TreeMap<String, Object>();
		params.put("assemblyConstituencyId", assemblyConstituencyId);
		params.put("nameUp", name.toUpperCase());
		PollingStation existingPollingStation = executeQueryGetObject("from PollingStation where assemblyConstituencyId=:assemblyConstituencyId and nameUp= :nameUp", params);
		return existingPollingStation;
	}

	/**
	 * @param pageInfo
	 * @return search result
	 * @throws AppException
	 */
	/*
	public List<PollingStation> getAllPollingStations() throws AppException
	{
		PageInfo pageInfo = null;
		return super.findObject(PollingStation.class, pageInfo).getResultList();
	}
	public List<PollingStation> getPollingStationOfState(long stateId) throws AppException{
		HibernateQueryParamPageInfo pageInfo = new HibernateQueryParamPageInfo();
		QueryParam stateQueryParam = new QueryParam();
		stateQueryParam.setCaseSenstive(false);
		stateQueryParam.setField("stateId");
		stateQueryParam.setFieldType(QueryParam.FIELD_TYPE_NUMBER);
		stateQueryParam.setOperator(QueryParam.OPERATOR_EQUAL);
		stateQueryParam.setValue(stateId);
		pageInfo.addCriteria(stateQueryParam);
		
		PageResult<PollingStation> pr = this.searchPollingStations(pageInfo);
		List<PollingStation> pollingStationList = pr.getResultList();
		return pollingStationList;
	}

	public void validateObjectForCreate(PollingStation pollingStation) throws AppException {
		checkIfStringMissing("Name", pollingStation.getName());
		checkIfObjectMissing("AssemblyConstituency", pollingStation.getAssemblyConstituency());
	}

	public void validateObjectForUpdate(PollingStation pollingStation) throws AppException {
		checkIfStringMissing("Name", pollingStation.getName());
		checkIfObjectMissing("AssemblyConstituency", pollingStation.getAssemblyConstituency());
	}
	private void checkIfPollingStationExistsWithSameName(PollingStation pollingStation) throws AppException{
		HibernateQueryParamPageInfo pageInfo = new HibernateQueryParamPageInfo();
		QueryParam emailQueryParam = new QueryParam();
		emailQueryParam.setCaseSenstive(false);
		emailQueryParam.setField("nameUp");
		emailQueryParam.setFieldType(QueryParam.FIELD_TYPE_STRING);
		emailQueryParam.setOperator(QueryParam.OPERATOR_EQUAL);
		emailQueryParam.setValue(pollingStation.getName().toUpperCase());
		pageInfo.addCriteria(emailQueryParam);
		
		QueryParam stateQueryParam = new QueryParam();
		stateQueryParam.setField("assemblyConstituencyId");
		stateQueryParam.setFieldType(QueryParam.FIELD_TYPE_NUMBER);
		stateQueryParam.setOperator(QueryParam.OPERATOR_EQUAL);
		stateQueryParam.setValue(pollingStation.getAssemblyConstituency().getId());
		pageInfo.addCriteria(stateQueryParam);
		
		if(pollingStation.getId() != null && pollingStation.getId() > 0){
			QueryParam pollingStationIdQueryParam = new QueryParam();
			pollingStationIdQueryParam.setCaseSenstive(false);
			pollingStationIdQueryParam.setField("id");
			pollingStationIdQueryParam.setFieldType(QueryParam.FIELD_TYPE_NUMBER);
			pollingStationIdQueryParam.setOperator(QueryParam.OPERATOR_NOT_EQUAL);
			pollingStationIdQueryParam.setValue(pollingStation.getId());
			pageInfo.addCriteria(pollingStationIdQueryParam);
		}
		
		PageResult<PollingStation> pr = this.searchPollingStations(pageInfo);
		if(pr.getResultList().size() > 0){
			throw new AppException("PollingStation already exists with name = "+pollingStation.getName()+" in state "+pollingStation.getAssemblyConstituency().getName());
		}
	}
	*/
}