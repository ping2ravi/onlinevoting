package com.next.vote.server.controller.jsf;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.faces.event.AjaxBehaviorEvent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.google.gdata.util.common.base.StringUtil;
import com.next.vote.client.dto.AssemblyConstituencyDto;
import com.next.vote.client.dto.StateDto;
import com.next.vote.client.dto.UserDto;
import com.next.vote.server.controller.BaseController;
import com.next.vote.server.service.VotingService;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLActions;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

@Component
@Scope("session")
@URLMappings(mappings = { @URLMapping(id = "userProfileBean", pattern = "/profile", viewId = "/WEB-INF/jsf/v2/userprofile.xhtml") })
public class UserProfileBean extends BaseJsfBean {

	private static final long serialVersionUID = 1L;

	private UserDto loggedInUser;
	private List<StateDto> stateList;
	private Long selectedStateId;
	private List<AssemblyConstituencyDto> assemblyConstituencyList;
	private Long selectedAssemblyConstituencyId;
	private boolean enableAssemblyConstituencyCombo = false;
	private String dateOfBirth;

	@Autowired
	private VotingService votingService;

	@URLActions(actions = { @URLAction(mappingId = "userProfileBean") })
	public void init() throws Exception {
		loggedInUser = getLoggedInUser(true,buildLoginUrl("/profile"));
		selectedStateId = 10L;
		if(loggedInUser != null){
			/*
			selectedStateId = loggedInUser.getStateLivingId();
			if(selectedStateId == null){
				
			}
			*/
			assemblyConstituencyList = votingService.getAllAssemblyConstituenciesOfState(selectedStateId);
			selectedAssemblyConstituencyId = loggedInUser.getAssemblyConstituencyVotingId();
			enableAssemblyConstituencyCombo = true;
			if(loggedInUser.getDateOfBirth() != null){
				dateOfBirth = "";
				if(loggedInUser.getDateOfBirth().getDate() < 10){
					dateOfBirth = dateOfBirth+"0";
				}
				dateOfBirth = dateOfBirth + loggedInUser.getDateOfBirth().getDate() +"/";
				if(loggedInUser.getDateOfBirth().getMonth() < 10){
					dateOfBirth = dateOfBirth+"0";
				}
				dateOfBirth = dateOfBirth + loggedInUser.getDateOfBirth().getMonth() +"/";
				
				dateOfBirth = dateOfBirth + (loggedInUser.getDateOfBirth().getYear() + 1900);
				
			}
		}else{
			enableAssemblyConstituencyCombo = false;
		}
		/*
		if(stateList == null || stateList.isEmpty()){
			stateList = votingService.getAllStates();
		}
		*/
		if(assemblyConstituencyList == null || assemblyConstituencyList.isEmpty()){
			assemblyConstituencyList = votingService.getAllAssemblyConstituenciesOfState(selectedStateId);
		}
	}

	public void saveProfile() {
		/*
		if(selectedStateId == null || selectedStateId ==0 || selectedStateId == 36){
			sendErrorMessageToJsfScreen("Please select State where you registered as Voter");
		}
		*/
		if(selectedAssemblyConstituencyId == null || selectedAssemblyConstituencyId ==0 ){
			sendErrorMessageToJsfScreen("Please select Assembly Constituency where you registered as Voter");
		}
		Calendar dobCalendar = getDateOfBirthAsDate();
		if(dobCalendar == null){
			sendErrorMessageToJsfScreen("Please enter your date of birth to be eligible to vote");
		}else{
			Calendar todayCalendar = Calendar.getInstance();
			todayCalendar.add(Calendar.YEAR, -18);
			if(dobCalendar.after(todayCalendar)){
				sendErrorMessageToJsfScreen("You must be 18 to be eligible for vote");
			}
		}
		if(StringUtil.isEmptyOrWhitespace(loggedInUser.getName())){
			sendErrorMessageToJsfScreen("Please enter your full name");
		}
		if(isValidInput()){
			loggedInUser.setAssemblyConstituencyVotingId(selectedAssemblyConstituencyId);
			//loggedInUser.setStateVotingId(selectedStateId);
			loggedInUser.setDateOfBirth(dobCalendar.getTime());
			
			loggedInUser = votingService.saveUser(loggedInUser);
			ssaveLoggedInUserInSession(loggedInUser);
			sendInfoMessageToJsfScreen("Profile updated succesfully. If you have already voted and you have updated your location, then you may want to re vote.");
		}
		String url = BaseController.getFinalRedirectUrlFromSesion(getHttpServletRequest());
		if(!StringUtil.isEmpty(url)){
			BaseController.clearFinalRedirectUrlInSesion(getHttpServletRequest());
			redirect(buildUrl(url));
		}
	}

	public void handleStateChange(AjaxBehaviorEvent event) {
		try {
			if(selectedStateId == 0 || selectedStateId == 36){
				enableAssemblyConstituencyCombo = false;
				assemblyConstituencyList = new ArrayList<>();
			}else{
				enableAssemblyConstituencyCombo = true;
				assemblyConstituencyList = votingService.getAllAssemblyConstituenciesOfState(selectedStateId);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public UserDto getLoggedInUser() {
		return loggedInUser;
	}

	public void setLoggedInUser(UserDto loggedInUser) {
		this.loggedInUser = loggedInUser;
	}

	public List<StateDto> getStateList() {
		return stateList;
	}

	public void setStateList(List<StateDto> stateList) {
		this.stateList = stateList;
	}

	public Long getSelectedStateId() {
		return selectedStateId;
	}

	public void setSelectedStateId(Long selectedStateId) {
		this.selectedStateId = selectedStateId;
	}

	public List<AssemblyConstituencyDto> getAssemblyConstituencyList() {
		return assemblyConstituencyList;
	}

	public void setAssemblyConstituencyList(
			List<AssemblyConstituencyDto> assemblyConstituencyList) {
		this.assemblyConstituencyList = assemblyConstituencyList;
	}

	public Long getSelectedAssemblyConstituencyId() {
		return selectedAssemblyConstituencyId;
	}

	public void setSelectedAssemblyConstituencyId(
			Long selectedAssemblyConstituencyId) {
		this.selectedAssemblyConstituencyId = selectedAssemblyConstituencyId;
	}

	public VotingService getVotingService() {
		return votingService;
	}

	public void setVotingService(VotingService votingService) {
		this.votingService = votingService;
	}

	public boolean isEnableAssemblyConstituencyCombo() {
		return enableAssemblyConstituencyCombo;
	}

	public void setEnableAssemblyConstituencyCombo(
			boolean enableAssemblyConstituencyCombo) {
		this.enableAssemblyConstituencyCombo = enableAssemblyConstituencyCombo;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}
	public Calendar getDateOfBirthAsDate() {
		if(StringUtil.isEmptyOrWhitespace(dateOfBirth)){
			return null;
		}
		String[] ddmmyyyy = dateOfBirth.split("/");
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DATE, Integer.parseInt(ddmmyyyy[0]));
		cal.set(Calendar.MONTH, Integer.parseInt(ddmmyyyy[1]) - 1);
		cal.set(Calendar.YEAR, Integer.parseInt(ddmmyyyy[2]));
		return cal;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

}
