package com.next.vote.server.persistance.dao;

import java.util.List;

import com.next.vote.server.persistance.Vote;

public interface VoteDao {

	public abstract Vote saveVote(Vote state);

	public abstract void deleteVote(Vote state);

	public abstract Vote getVoteById(Long id);

	public abstract List<Vote> getAllVotes();

	public abstract List<Vote> getAllVotesByAc(Long assemblyConstituencyId);
	
	public abstract List<Vote> getAllVotesByAcAndElection(Long assemblyConstituencyId,Long electionId, int pageSize);
	
	public abstract List<Vote> getAllVotesByElection(Long electionId, int pageSize);

	public abstract List<Vote> getAllVotesByCandidate(Long candidateId);

	public abstract List<Vote> getAllVotesByParty(Long partyId);
	
	public abstract Vote getVotesByUserForElection(Long userId, Long electionId);

}