package com.next.vote.server.util;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.next.vote.client.dto.AssemblyConstituencyDto;
import com.next.vote.client.dto.DistrictDto;
import com.next.vote.client.dto.PollingStationDto;
import com.next.vote.client.dto.StateDto;
import com.next.vote.server.service.VotingService;

public class LocationDataDownloader {

	private static Map<String, String> stateNameMap = new HashMap<String, String>();
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		stateNameMap.put("Andhra Pradesh", "Andhra Pradesh");
		stateNameMap.put("Dadra & Nagar Haveli", "Dadra and Nagar Haveli(UT)");
		stateNameMap.put("Daman & Diu", "Daman and Diu(UT)");
		stateNameMap.put("Goa", "Goa");
		stateNameMap.put("Gujarat", "Gujarat");
		stateNameMap.put("Haryana", "Haryana");
		stateNameMap.put("Himachal Pradesh", "Himachal Pradesh");
		stateNameMap.put("Jharkhand", "Jharkhand");
		stateNameMap.put("Madhya Pradesh", "Madhya Pradesh");
		stateNameMap.put("Meghalaya", "Meghalaya");
		stateNameMap.put("Mizoram", "Mizoram");
		stateNameMap.put("NCT OF Delhi", "Delhi");
		stateNameMap.put("Orissa", "Odisha");
		
		stateNameMap.put("Puducherry", "Puducherry");
		stateNameMap.put("Rajasthan", "Rajasthan");
		stateNameMap.put("Sikkim", "Sikkim");
		stateNameMap.put("Tripura", "Tripura");
		stateNameMap.put("West Bengal", "West Bengal");
		ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("/spring/voting-core.xml");
		VotingService votingService = applicationContext.getBean(VotingService.class);
		System.out.println("voting Service "+votingService);
		downloadAllData(votingService);
		applicationContext.close();
	}
	private static StateDto createState(VotingService votingService, String name){
		name = stateNameMap.get(name);
		StateDto stateDto = new StateDto();
		stateDto.setName(name);
		stateDto = votingService.saveState(stateDto);
		return stateDto;
	}
	private static DistrictDto createDistrict(VotingService votingService, String name, StateDto stateDto){
		name = removeCountNumber(name);
		System.out.println("name="+name);
		DistrictDto districtDto = new DistrictDto();
		districtDto.setName(name);
		districtDto.setStateId(stateDto.getId());
		districtDto = votingService.saveDistrict(districtDto);
		
		
		return districtDto;
	}
	
	private static AssemblyConstituencyDto createAssemblyConstituency(VotingService votingService, String name, DistrictDto districtDto){
		name = removeCountNumber(name);

		AssemblyConstituencyDto assemblyConstituencyDto = new AssemblyConstituencyDto();
		assemblyConstituencyDto.setName(name);
		assemblyConstituencyDto.setDistrictId(districtDto.getId());
		assemblyConstituencyDto = votingService.saveAssemblyConstituency(assemblyConstituencyDto);
		return assemblyConstituencyDto;
	}
	private static PollingStationDto createPollingBooth(VotingService votingService, String name, AssemblyConstituencyDto assemblyConstituencyDto){
		name = removeCountNumber(name);

		PollingStationDto pollingStationDto = new PollingStationDto();
		pollingStationDto.setName(name);
		pollingStationDto.setAssemblyConstituencyId(assemblyConstituencyDto.getId());
		pollingStationDto = votingService.savePollingStation(pollingStationDto);
		
		return pollingStationDto;
	}
		
	private static String removeCountNumber(String name){
		StringBuffer sb = new StringBuffer();
		char oneChar;
		boolean nameStarted = false;
		for(int i=0; i < name.length();i++ ){
			oneChar = name.charAt(i);
			if(!nameStarted){
				if(oneChar == '-' || oneChar == ' ' || (oneChar >= '0' && oneChar <= '9')){
					continue;
				}
			}
			nameStarted = true;
			sb.append(oneChar);
		}
		return sb.toString();
	}
	private static void downloadAllData(VotingService votingService){
		WebDriver webDriver = new FirefoxDriver();
		webDriver.get("http://www.eci-polldaymonitoring.nic.in/psl/default.aspx");
		//read all states in combo box
		//WebElement stateComboBox = webDriver.findElement(By.name("ddlState"));
		Select stateComboBox = new Select(webDriver.findElement(By.id("ddlState")));
		Select districtComboBox;
		Select acComboBox;
		Select pbComboBox;
		int stateCount = 1;
		int districtCount = 1;
		int totalDistricts;
		int acCount = 1;
		int pbCount = 1;
		int totalAcs;
		int totalPbs;
		int totalStates = stateComboBox.getOptions().size();
		WebElement oneStateOption;
		WebElement oneDistrictOption;
		WebElement oneAcOption;
		WebElement onePbOption;
		
		String stateName;
		StateDto state;
		String districtName;
		DistrictDto districtDto;
		String acName;
		AssemblyConstituencyDto assemblyConstituencyDto;
		String pbName;
		while(stateCount < totalStates ){
			stateComboBox = new Select(webDriver.findElement(By.id("ddlState")));
			oneStateOption = stateComboBox.getOptions().get(stateCount);
			System.out.println("Working on State " + oneStateOption.getText());
			stateName = oneStateOption.getText();
			state = createState(votingService, stateName);
			stateComboBox.selectByIndex(stateCount);
			
			sleep(1000);
			
			districtComboBox = new Select(webDriver.findElement(By.id("ddlDistrict")));
			totalDistricts = districtComboBox.getOptions().size();
			districtCount = 1;
			while(districtCount < totalDistricts){
				districtComboBox = new Select(webDriver.findElement(By.id("ddlDistrict")));
				oneDistrictOption = districtComboBox.getOptions().get(districtCount);
				System.out.println("     Working on District " + oneDistrictOption.getText());
				districtName = oneDistrictOption.getText();
				districtComboBox.selectByIndex(districtCount);
				districtDto = createDistrict(votingService, districtName, state);
				
				sleep(1000);
				acComboBox = new Select(webDriver.findElement(By.id("ddlAC")));
				totalAcs = acComboBox.getOptions().size();
				acCount = 1;
				while(acCount < totalAcs){
					//acComboBox = new Select(webDriver.findElement(By.id("ddlAC")));
					oneAcOption = acComboBox.getOptions().get(acCount);
					System.out.println("          Working on AC " + oneAcOption.getText());
					acName = oneAcOption.getText();
					//acComboBox.selectByIndex(acCount);

					assemblyConstituencyDto = createAssemblyConstituency(votingService, acName, districtDto);
					//sleep(1000);
					/*
					pbComboBox = new Select(webDriver.findElement(By.id("ddlPS")));
					totalPbs = pbComboBox.getOptions().size();
					pbCount = 1;
					for(WebElement onePb:pbComboBox.getOptions()){
						pbName = onePb.getText();
						System.out.println("               Working on Pollingbooth " + pbName);
						//createPollingBooth(votingService, pbName, assemblyConstituencyDto);
					}
					*/
					/*
					while(pbCount < totalPbs){
						pbComboBox = new Select(webDriver.findElement(By.id("ddlPS")));
						onePbOption = pbComboBox.getOptions().get(pbCount);
						System.out.println("          Working on Pollingbooth " + onePbOption.getText());
						//pbComboBox.selectByIndex(pbCount);
						
						pbCount++;
					}
					*/
					
					acCount++;
				}
				districtCount++;
			}
			stateCount++;
		}
		
	}
	private static void sleep(long millis){
		try {
			Thread.sleep(millis * 2);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
