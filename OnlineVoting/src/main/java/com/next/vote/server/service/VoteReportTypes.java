package com.next.vote.server.service;

public class VoteReportTypes {

	
	public static final String TOTAL_ELECTION_VOTE = "TotalElectionVote";
	
	public static final String TOTAL_ELECTION_PUBLIC_VOTE = "TotalElectionPublicVote";
	
	public static final String TOTAL_ELECTION_PRIVATE_VOTE = "TotalElectionPrivateVote";
	
	public static final String TOTAL_ELECTION_MALE_VOTE = "TotalElectionMaleVote";
	
	public static final String TOTAL_ELECTION_FEMALE_VOTE = "TotalElectionFemaleVote";
	
	public static final String TOTAL_ELECTION_NOT_SPECIFIED_VOTE = "TotalElectionNotSpecifiedVote";
	
	public static final String TOTAL_ELECTION_AGE_VOTE_UNKNOWN = "TotalElectionAgeUnknown";
	
	public static final String TOTAL_ELECTION_AGE_VOTE = "TotalElectionAge";
	
	public static final String TOTAL_ELECTION_PARTY_VOTE = "TotalElectionPartyVote";
	
	public static final String TOTAL_ELECTION_PARTY_PUBLIC_VOTE = "TotalElectionPartyPublicVote";
	
	public static final String TOTAL_ELECTION_PARTY_PRIVATE_VOTE = "TotalElectionPartyPrivateVote";

	public static final String TOTAL_CANDIDATE_VOTE = "TotalCandidateVote";

	public static final String TOTAL_CANDIDATE_PUBLIC_VOTE = "TotalCandidatePublicVote";
	
	public static final String TOTAL_CANDIDATE_PRIVATE_VOTE = "TotalCandidatePrivateVote";
	

	public static final String TOTAL_AC_VOTE = "TotalAcVote";

	public static final String TOTAL_AC_PUBLIC_VOTE = "TotalAcPublicVote";
	
	public static final String TOTAL_AC_PRIVATE_VOTE = "TotalAcPrivateVote";

	public static final String TOTAL_AC_MALE_VOTE = "TotalAcMaleVote";
	
	public static final String TOTAL_AC_FEMALE_VOTE = "TotalAcFemaleVote";
	
	public static final String TOTAL_AC_NOT_SPECIFIED_VOTE = "TotalAcNotSpecifiedVote";

	public static final String TOTAL_AC_AGE_VOTE_UNKNOWN = "TotalAcAgeUnknown";
	
	public static final String TOTAL_AC_AGE_VOTE = "TotalAcAge";

}
