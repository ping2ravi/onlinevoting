package com.next.vote.web.util;

import javax.servlet.http.HttpServletRequest;

import com.next.vote.client.dto.UserDto;

public class VotingSessionUtil {

	private static final String USER_SESSION_IDENTIFIER = "User";

	public static void saveUserInSession(HttpServletRequest httpServletRequest, UserDto user){
		httpServletRequest.getSession(true).setAttribute(USER_SESSION_IDENTIFIER, user);
	}
	public static UserDto getUserFromSession(HttpServletRequest httpServletRequest){
		return (UserDto)httpServletRequest.getSession().getAttribute(USER_SESSION_IDENTIFIER);
	}
	public static void removeUserFromSession(HttpServletRequest httpServletRequest){
		httpServletRequest.getSession(true).removeAttribute(USER_SESSION_IDENTIFIER);
	}
	
}
