package com.next.vote.client.dto;

import java.util.Date;


public class FacebookAccountDto {

	private Long id;
	private String token;
	private Date tokenValidTill;
	private String userName;
	private Long userId;
	private String email;
	private String facebookUserId;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public Date getTokenValidTill() {
		return tokenValidTill;
	}
	public void setTokenValidTill(Date tokenValidTill) {
		this.tokenValidTill = tokenValidTill;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFacebookUserId() {
		return facebookUserId;
	}
	public void setFacebookUserId(String facebookUserId) {
		this.facebookUserId = facebookUserId;
	}

	

}
