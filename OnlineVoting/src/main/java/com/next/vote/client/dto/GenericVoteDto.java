package com.next.vote.client.dto;

import java.io.Serializable;

public class GenericVoteDto implements Serializable{

	private static final long serialVersionUID = 1L;

	private String description;
	private int totalVotes;
	private double votePercent;
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getTotalVotes() {
		return totalVotes;
	}
	public void setTotalVotes(int totalVotes) {
		this.totalVotes = totalVotes;
	}
	public double getVotePercent() {
		return votePercent;
	}
	public void setVotePercent(double votePercent) {
		this.votePercent = votePercent;
	}
	

}
