package com.next.vote.client.exception;

public class VotingException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public VotingException() {
	}

	public VotingException(String message) {
		super(message);
	}

	public VotingException(Throwable cause) {
		super(cause);
	}

	public VotingException(String message, Throwable cause) {
		super(message, cause);
	}

	public VotingException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
