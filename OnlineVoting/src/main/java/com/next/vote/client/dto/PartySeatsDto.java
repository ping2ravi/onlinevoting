package com.next.vote.client.dto;

import java.io.Serializable;

public class PartySeatsDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long partyId;
	private String partyName;
	private int totalSeats;
	
	public long getPartyId() {
		return partyId;
	}
	public void setPartyId(long partyId) {
		this.partyId = partyId;
	}
	public String getPartyName() {
		return partyName;
	}
	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}
	public int getTotalSeats() {
		return totalSeats;
	}
	public void setTotalSeats(int totalSeats) {
		this.totalSeats = totalSeats;
	}


}
