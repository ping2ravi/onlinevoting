package com.next.vote.client.dto;

import java.io.Serializable;

public class PartyVoteDto implements Serializable{

	private static final long serialVersionUID = 1L;

	private long partyId;
	private String partyName;
	private int totalVotes;
	private double votePercent;
	
	public long getPartyId() {
		return partyId;
	}
	public void setPartyId(long partyId) {
		this.partyId = partyId;
	}
	public String getPartyName() {
		return partyName;
	}
	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}
	public int getTotalVotes() {
		return totalVotes;
	}
	public void setTotalVotes(int totalVotes) {
		this.totalVotes = totalVotes;
	}
	public double getVotePercent() {
		return votePercent;
	}
	public void setVotePercent(double votePercent) {
		this.votePercent = votePercent;
	}
	

}
