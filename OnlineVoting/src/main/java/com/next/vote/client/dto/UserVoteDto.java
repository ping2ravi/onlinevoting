package com.next.vote.client.dto;

import java.io.Serializable;
import java.util.Date;


public class UserVoteDto implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String name;
	private String votingMessage;
	private String profilePic;
	private Date votingTime;
	private boolean publicVote;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getVotingMessage() {
		return votingMessage;
	}
	public void setVotingMessage(String votingMessage) {
		this.votingMessage = votingMessage;
	}
	public String getProfilePic() {
		return profilePic;
	}
	public void setProfilePic(String profilePic) {
		this.profilePic = profilePic;
	}
	public Date getVotingTime() {
		return votingTime;
	}
	public void setVotingTime(Date votingTime) {
		this.votingTime = votingTime;
	}
	public boolean isPublicVote() {
		return publicVote;
	}
	public void setPublicVote(boolean publicVote) {
		this.publicVote = publicVote;
	}


}
