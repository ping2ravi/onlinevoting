package com.next.vote.client.dto;


public class TwitterAccountDto {

	private Long id;
	private String token;
	private String tokenSecret;
	private Long userId;
	private Long twitterId;
	private String screenName;
	private String screenNameCap;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getTokenSecret() {
		return tokenSecret;
	}
	public void setTokenSecret(String tokenSecret) {
		this.tokenSecret = tokenSecret;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Long getTwitterId() {
		return twitterId;
	}
	public void setTwitterId(Long twitterId) {
		this.twitterId = twitterId;
	}
	public String getScreenName() {
		return screenName;
	}
	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}
	public String getScreenNameCap() {
		return screenNameCap;
	}
	public void setScreenNameCap(String screenNameCap) {
		this.screenNameCap = screenNameCap;
	}


}
