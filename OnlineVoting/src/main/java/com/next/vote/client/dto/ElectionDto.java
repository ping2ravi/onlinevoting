package com.next.vote.client.dto;

import java.util.Date;

public class ElectionDto {
	private Long id;
	private String name;
	private Date electionDate;


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Date getElectionDate() {
		return electionDate;
	}


	public void setElectionDate(Date electionDate) {
		this.electionDate = electionDate;
	}


}